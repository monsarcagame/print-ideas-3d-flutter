function generateMicrodataTag(shopItemData) {
    const microdataTag = document.getElementById("ProductMicrodataTag");
    var json = {
        "@context":"https://schema.org",
        "@type":"Product",
        "productID":"facebook_tshirt_001",
        "name":"Facebook T-Shirt",
        "description":"Unisex Facebook T-shirt, Small",
        "url":"https://example.org/facebook",
        "image":"https://example.org/facebook.jpg",
        "brand":"facebook",
        "offers": [
          {
            "@type": "Offer",
            "price": "7.99",
            "priceCurrency": "USD",
            "itemCondition": "https://schema.org/NewCondition",
            "availability": "https://schema.org/InStock"
          }
        ],
        "additionalProperty": [{
          "@type": "PropertyValue",
          "propertyID": "item_group_id",
          "value": "fb_tshirts"
        }]
      };
    var script = document.createElement('script');
    script.type = "application/ld+json";
    script.text = JSON.stringify(json);

    document.querySelector('body').appendChild(script);

    //microdataTag.textContent = JSON.stringify(shopItemData);
    //microdataTag.innerHTML = shopItemData;
    //microdataTag.innerHTML = json;
}