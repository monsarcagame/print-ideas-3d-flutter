// ignore: avoid_web_libraries_in_flutter

import 'package:flutter/material.dart';
import 'package:print_ideas_3d/models/core/business_model.dart';

import 'package:provider/provider.dart';
import 'package:model_viewer_plus/model_viewer_plus.dart';

/// Visualiza el modelo 3d de un Shop item.
class Home3DModel extends StatelessWidget {
  const Home3DModel({Key? key, required this.modelId}) : super(key: key);

  final String modelId;

  @override
  Widget build(BuildContext context) {
    final String model3dUrl = Provider.of<BusinessModel>(context).model3dUrl;

    return ModelViewer(src: model3dUrl);
  }
}
