import 'package:auto_size_text_pk/auto_size_text_pk.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:print_ideas_3d/constants/text_styles.dart';
import 'package:print_ideas_3d/models/core/app_state_model.dart';
import 'package:print_ideas_3d/router/ui_pages.dart';
import 'package:provider/provider.dart';

class HomeCard extends StatelessWidget {
  const HomeCard(
      {Key? key,
      this.cardTitle,
      this.cardText,
      this.cardImagePath,
      required this.destination})
      : super(key: key);

  final String? cardTitle;
  final String? cardText;
  final String? cardImagePath;

  /// Página de destino cuando se hace click en la tarjeta.
  final Pages destination;

  @override
  Widget build(BuildContext context) {
    // Get app state.
    final appState = Provider.of<AppStateModel>(context, listen: false);

    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // Card image
          Expanded(
            child: SizedBox(
              height: double.infinity,
              width: double.infinity,
              child: ClipRRect(
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(4.0),
                  topRight: Radius.circular(4.0),
                ),
                child: FittedBox(
                  fit: BoxFit.cover,
                  clipBehavior: Clip.hardEdge,
                  child: Image.asset(
                    cardImagePath!,
                    fit: BoxFit.fitWidth,
                  ),
                ),
              ),
            ),
          ),
          // Info.
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // Title & description.
                  Expanded(
                    flex: 2,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // Title.
                        Text(
                          cardTitle!,
                          textAlign: TextAlign.left,
                          style: const TextStyle(
                            color: kTextColorCardTitle,
                            fontWeight: FontWeight.bold,
                            fontSize: kTextSizeCardTitle,
                          ),
                        ),
                        // Description.
                        AutoSizeText(
                          cardText!,
                          style: const TextStyle(
                            color: kTextColorCardDefault,
                            fontSize: kTextSizeCardDefault,
                            fontWeight: FontWeight.w300,
                          ),
                          textAlign: TextAlign.left,
                          maxLines: 4,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ],
                    ),
                  ),
                  // Action button
                  Expanded(
                    flex: 1,
                    child: Center(
                      child: ElevatedButton(
                        onPressed: () {
                          context.goNamed(destination.name);
                          appState.currentPage = destination;
                        },
                        child: const Text('Más info'),
                      ),
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
