import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:print_ideas_3d/router/ui_pages.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'navbar_item.dart';
import 'navbar_logo.dart';

/// Barra de navegación para tablets y escritorio.
class NavigationBarTabletDesktop extends StatelessWidget {
  final List<Pages> items;

  const NavigationBarTabletDesktop({Key? key, required this.items})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (context, sizingInfo) {
      return Row(
        children: [
          // Navbar logo.
          const SizedBox(child: NavBarLogo()),
          // Navbar pages.
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Expanded(
                  child: Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: List.generate(
                            items.length,
                            (index) {
                              return NavBarItem(
                                onTap: () {
                                  context.goNamed(items[index].name);
                                },
                                item: items[index],
                              );
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          //
        ],
      );
    });
  }
}
