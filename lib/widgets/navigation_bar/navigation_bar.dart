import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:print_ideas_3d/constants/app_colors.dart';
import 'package:print_ideas_3d/helpers/url_helper.dart';
import 'package:print_ideas_3d/models/core/app_state_model.dart';
import 'package:print_ideas_3d/models/core/business_model.dart';
import 'package:print_ideas_3d/router/ui_pages.dart';
import 'package:provider/provider.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'navbar_item.dart';
import 'navbar_logo.dart';

/// Map from "Pages" enum to "String". Define un nombre leíble para el usuario.
const Map<Pages, String> navbarName = {
  Pages.home: 'Inicio',
  Pages.works: 'Trabajos',
  Pages.services: 'Servicios',
  Pages.shop: 'Tienda',
  Pages.cart: 'Carrito de compras',
  Pages.addProduct: 'Agregar nuevo producto',
  Pages.about: 'Nosotros',
  Pages.contact: 'Contacto',
  Pages.adminLogin: 'Iniciar sesión como administrador',
  Pages.login: 'Iniciar sesión',
  Pages.signIn: 'Resgistrarse'
};

/// Lista de páginas para la barra de navegación en tablets y desktops.
final navbarItems = [Pages.works, Pages.services, Pages.about, Pages.contact];

/// Barra de navegación responsiva.
class ResponsiveNavigationBar {
  /// AppBar para móviles.
  static AppBar appBarMobile(BuildContext context) {
    AppStateModel appState = Provider.of<AppStateModel>(context);
    BusinessModel business = Provider.of<BusinessModel>(context);

    return AppBar(
      title: Text(
        navbarName[appState.currentPage]!,
        textAlign: TextAlign.center,
        style: const TextStyle(fontWeight: FontWeight.bold),
      ),
      centerTitle: true,
      titleSpacing: 0.0,
      actions: [
        IconButton(
          onPressed: () {
            UrlHelper.launchURL(business.shopUrl);
          },
          icon: const Icon(
            FontAwesomeIcons.shop,
            color: kSecondaryColor,
          ),
        ),
        const SizedBox(width: 10.0),
        IconButton(
          onPressed: () {
            UrlHelper.launchURL(business.instagramPage);
          },
          icon: const Icon(Icons.photo_camera_outlined),
        ),
        IconButton(
          onPressed: () {
            UrlHelper.launchURL(business.facebookPage);
          },
          icon: const Icon(Icons.facebook),
        ),
        IconButton(
          onPressed: () {
            UrlHelper.openwhatsapp(context, business.whatsappNumber, "Hola!");
          },
          icon: const Icon(FontAwesomeIcons.whatsapp),
        )
      ],
    );
  }

  ///
  static AppBar appBarTabletDesktop(
      BuildContext context, double preferredHeight) {
    AppStateModel appState = Provider.of<AppStateModel>(context);
    BusinessModel business = Provider.of<BusinessModel>(context);

    return AppBar(
      toolbarHeight: preferredHeight,
      leadingWidth: 200.0,
      leading: const NavBarLogo(),
      //title: Text(business.displayName),
      actions: [
        ...List.generate(
          navbarItems.length,
          (index) {
            return NavBarItem(
              onTap: () {
                context.goNamed(navbarItems[index].name);
                appState.currentPage = navbarItems[index];
              },
              item: navbarItems[index],
            );
          },
        ),
        //
        const SizedBox(
          width: 25.0,
        ),
        //
        IconButton(
          onPressed: () {
            UrlHelper.launchURL(business.instagramPage);
          },
          icon: const Icon(Icons.photo_camera_outlined),
        ),
        IconButton(
          onPressed: () {
            UrlHelper.launchURL(business.facebookPage);
          },
          icon: const Icon(Icons.facebook),
        ),
        IconButton(
          onPressed: () {
            UrlHelper.openwhatsapp(context, business.whatsappNumber, "Hola!");
          },
          icon: const Icon(FontAwesomeIcons.whatsapp),
        ),
        const SizedBox(width: 10.0),
        // Shop button
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10.0),
          child: ElevatedButton.icon(
            onPressed: () {
              UrlHelper.launchURL(business.shopUrl);
            },
            label: const Padding(
              padding: EdgeInsets.only(left: 8.0),
              child: Text('Ir a la tienda'),
            ),
            icon: const Icon(FontAwesomeIcons.shop),
          ),
        ),
        const SizedBox(width: 5.0)
      ],
    );
  }
}
