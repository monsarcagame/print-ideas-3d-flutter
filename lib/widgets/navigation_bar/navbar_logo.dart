import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:print_ideas_3d/router/ui_pages.dart';
import 'package:provider/provider.dart';
import 'package:responsive_builder/responsive_builder.dart';

import '../../models/core/app_state_model.dart';
import '../../models/core/business_model.dart';

class NavBarLogo extends StatelessWidget {
  const NavBarLogo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AppStateModel appState = Provider.of<AppStateModel>(context);
    BusinessModel business = Provider.of<BusinessModel>(context);

    return ResponsiveBuilder(builder: (context, sizingInfo) {
      return InkWell(
        child: Row(
          children: [
            Container(
              padding: const EdgeInsets.all(8),
              child: Image.asset('assets/images/print3d_logo.png'),
            ),
            Text(business.displayName),
          ],
        ),
        onTap: () {
          context.goNamed(Pages.home.name);
          appState.currentPage = Pages.home;
        },
      );
    });
  }
}
