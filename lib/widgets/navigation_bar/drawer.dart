import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:print_ideas_3d/constants/text_styles.dart';
import 'package:print_ideas_3d/models/core/app_state_model.dart';
import 'package:print_ideas_3d/router/ui_pages.dart';
import 'package:print_ideas_3d/widgets/navigation_bar/navigation_bar.dart';
import 'package:print_ideas_3d/widgets/platform_views/pointer_interceptor_web.dart';
import 'package:provider/provider.dart';

/// Items que estarán en el drawer.
const drawerItems = [
  NavbarDrawerItem(page: Pages.home),
  NavbarDrawerItem(page: Pages.services),
  NavbarDrawerItem(page: Pages.about),
];

/// Map que asocia páginas con su correspondiente BottomNavigationBarItem.
const bottomNavBarItems = <Pages, BottomNavigationBarItem>{
  Pages.home: BottomNavigationBarItem(label: "Inicio", icon: Icon(Icons.home)),
  //Pages.shop: BottomNavigationBarItem(label: "Tienda", icon: Icon(Icons.shop)),
  Pages.works:
      BottomNavigationBarItem(label: "Trabajos", icon: Icon(Icons.work)),
  Pages.contact: BottomNavigationBarItem(
      label: "Contáctanos", icon: Icon(Icons.contact_support))
};

/// Drawer para móviles.
class AppDrawer extends StatelessWidget {
  const AppDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PointerInterceptor(
      child: const Drawer(
        child: Column(
          children: [
            ...drawerItems,
            // Footer Drawer
            SizedBox(
              height: 10.0,
            ),
            ListTile(
              title: Text(
                'Print.Ideas3D',
                style: TextStyle(color: kTextColorDefault),
              ),
            )
          ],
        ),
      ),
    );
  }
}

/// Drawer item.
class NavbarDrawerItem extends StatelessWidget {
  final String? title;
  final Pages page;
  final Function? onTapDrawerItem;

  const NavbarDrawerItem(
      {Key? key, this.title, required this.page, this.onTapDrawerItem})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 5.0),
      child: ListTile(
        title: Text(
          "${navbarName[page]}",
          style: const TextStyle(color: kTextColorNavBar),
        ),
        onTap: () {
          // Close the drawer.
          Navigator.pop(context);
          // Si != null call onTapDrawerItem.
          if (onTapDrawerItem != null) {
            onTapDrawerItem!();
          }

          context.goNamed(page.name);
          Provider.of<AppStateModel>(context, listen: false).currentPage = page;
        },
      ),
    );
  }
}
