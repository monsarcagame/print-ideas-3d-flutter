import 'package:flutter/material.dart';
import 'package:print_ideas_3d/constants/app_colors.dart';
import 'package:print_ideas_3d/constants/text_styles.dart';
import 'package:print_ideas_3d/models/core/app_state_model.dart';
import 'package:print_ideas_3d/router/ui_pages.dart';
import 'package:print_ideas_3d/widgets/navigation_bar/navigation_bar.dart';
import 'package:provider/provider.dart';

class NavBarItem extends StatefulWidget {
  final Pages? item;
  final Function? onTap;

  const NavBarItem({Key? key, this.item, this.onTap}) : super(key: key);

  @override
  _NavBarItemState createState() => _NavBarItemState();
}

class _NavBarItemState extends State<NavBarItem> {
  bool _isHovering = false;

  @override
  Widget build(BuildContext context) {
    return Consumer<AppStateModel>(builder: (context, appState, child) {
      return Container(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        decoration: widget.item == appState.currentPage
            ? const BoxDecoration(
                border: Border(
                    bottom: BorderSide(color: kPrimaryColor, width: 2.0)))
            : null,
        child: Center(
          child: InkWell(
            onHover: (value) {
              setState(() {
                _isHovering = value;
              });
            },
            child: Text(
              navbarName[widget.item!]!,
              style: TextStyle(
                  color: _isHovering ? kTextColorHovering : kTextColorNavBar,
                  fontSize: kTextSizeNavbarPage),
            ),
            onTap: () => widget.onTap!(),
          ),
        ),
      );
    });
  }
}
