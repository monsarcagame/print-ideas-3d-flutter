import 'package:flutter/material.dart';

class LogoPrint3D extends StatelessWidget {
  const LogoPrint3D({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(
          'print3d_logo.png',
          width: 60,
          height: 60,
        ),
      ],
    );
  }
}
