import 'package:flutter/material.dart';
import 'package:print_ideas_3d/models/core/app_state_model.dart';
import 'package:print_ideas_3d/router/ui_pages.dart';
import 'package:print_ideas_3d/widgets/dialogs/dialogs.dart';
import 'package:provider/provider.dart';

class OrdersButton extends StatelessWidget {
  const OrdersButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var appState = Provider.of<AppStateModel>(context, listen: true);
    return IconButton(
        tooltip: 'Ordenes de compras',
        onPressed: () {
          // Si el usuario esta logueado, puede ver el carrito.
          if (appState.printUser.isSignedIn) {
            appState.currentPage = Pages.merchantOrders;
          } else {
            showDialog(
                context: context,
                builder: (_) => Dialogs.messageDialog(
                        title: 'Print.Ideas3D',
                        message: 'Para ver tus compras debes iniciar sesión.',
                        actions: [
                          ElevatedButton(
                              onPressed: () {
                                Navigator.pop(context);
                                appState.currentPage = Pages.login;
                              },
                              child:
                                  const Text('Login', textAlign: TextAlign.end))
                        ]));
          }
        },
        icon: const Icon(Icons.shopping_bag_outlined));
  }
}
