import 'package:flutter/material.dart';
import 'package:print_ideas_3d/constants/app_colors.dart';
import 'package:print_ideas_3d/models/core/app_state_model.dart';
import 'package:print_ideas_3d/models/shop/cart_model.dart';
import 'package:print_ideas_3d/router/ui_pages.dart';
import 'package:print_ideas_3d/widgets/dialogs/dialogs.dart';
import 'package:provider/provider.dart';

class CartButton extends StatelessWidget {
  const CartButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AppStateModel appState = Provider.of<AppStateModel>(context, listen: true);
    Cart shopCart = Provider.of<Cart>(context);
    return IconButton(
      tooltip: 'Carrito de compras',
      icon: Center(
        child: SizedBox(
          width: 24.0,
          height: 24.0,
          child: Stack(
            clipBehavior: Clip.none,
            //fit: StackFit.passthrough,
            alignment: Alignment.center,
            children: [
              const Icon(Icons.shopping_cart_outlined),
              // Cart item count.
              Positioned(
                  bottom: 10.0,
                  right: 0.0,
                  child: Container(
                      padding: const EdgeInsets.all(2.0),
                      constraints:
                          const BoxConstraints(minWidth: 16.0, minHeight: 16.0),
                      decoration: const BoxDecoration(
                          color: kSecondaryColor, shape: BoxShape.circle),
                      child: Center(
                        child: Text(
                          shopCart.itemCount.toString(),
                          textAlign: TextAlign.center,
                        ),
                      ))),
            ],
          ),
        ),
      ),
      onPressed: () {
        // Si el usuario esta logueado, puede ver el carrito.
        if (appState.printUser.isSignedIn) {
          if (shopCart.isEmpty) {
            showDialog(
                context: context,
                builder: (_) => Dialogs.messageDialog(
                        title: 'Print.Ideas3D',
                        message: 'El carrito está vacio!',
                        actions: [
                          ElevatedButton(
                              onPressed: () {
                                Navigator.pop(context);
                                appState.currentPage = Pages.shop;
                              },
                              child: const Text('Ver shop',
                                  textAlign: TextAlign.end))
                        ]));
          } else {
            appState.currentPage = Pages.cart;
          }
        } else {
          showDialog(
              context: context,
              builder: (_) => Dialogs.messageDialog(
                      title: 'Print.Ideas3D',
                      message:
                          'Para ver el carrito primero debes iniciar sesión.',
                      actions: [
                        ElevatedButton(
                            onPressed: () {
                              Navigator.pop(context);
                              appState.currentPage = Pages.login;
                            },
                            child:
                                const Text('Login', textAlign: TextAlign.end))
                      ]));
        }
      },
    );
  }
}
