import 'package:flutter/material.dart';

enum AlertType { success, warning, info, error }

class Dialogs {
  /// Dialogo que muestra un mensaje y un estilo dependiendo del tipo de alerta.
  static Future<void> showMessageDialog(BuildContext context,
      {required Widget message,
      AlertType alertType = AlertType.info,
      void Function()? action}) async {
    return await showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          Icon alertIcon;
          switch (alertType) {
            case AlertType.success:
              alertIcon = const Icon(Icons.check_circle_outline_rounded,
                  color: Colors.green, size: 50.0);
              break;
            case AlertType.warning:
              alertIcon = const Icon(Icons.warning_amber_rounded,
                  color: Colors.yellow, size: 50.0);
              break;
            case AlertType.info:
              alertIcon = const Icon(Icons.info_outline_rounded,
                  color: Colors.blue, size: 50.0);
              break;
            case AlertType.error:
              alertIcon = const Icon(Icons.error_outline_rounded,
                  color: Colors.red, size: 50.0);
              break;
          }
          return AlertDialog(
              title: const Text('Print.Ideas3D'),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  alertIcon,
                  message,
                ],
              ),
              actions: [
                ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                    if (action != null) action();
                  },
                  child: const Text('Ok', textAlign: TextAlign.end),
                )
              ]);
        });
  }

  /// Show a loading dialog with global key for outside manipulation.
  static Future<void> showLoadingDialog(BuildContext context,
      {GlobalKey? key, String loadingMessage = 'Loading message'}) async {
    return await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          key: key,
          title: const Text('Print.Ideas3D'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [Text(loadingMessage), const LinearProgressIndicator()],
          ),
        );
      },
    );
  }

  /// Show a password input widget.
  static Future<String> showPasswordInputDialog(BuildContext context) async {
    return await showDialog(
        context: context,
        builder: (BuildContext context) {
          final passwordCtrl = TextEditingController();
          return AlertDialog(
            title: const Text('Password'),
            content: TextField(
              controller: passwordCtrl,
              obscureText: true,
            ),
            actions: [
              ElevatedButton(
                onPressed: () {
                  Navigator.of(context).pop(passwordCtrl.value.text);
                },
                child: const Text('Ok', textAlign: TextAlign.end),
              )
            ],
          );
        });
  }

  ///
  static AlertDialog messageDialog(
      {required String title, required String message, List<Widget>? actions}) {
    return AlertDialog(
        title: Text(title), content: Text(message), actions: actions);
  }
}
