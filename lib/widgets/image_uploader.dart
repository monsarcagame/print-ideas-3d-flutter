import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:print_ideas_3d/services/firebase_service.dart';

///
class ImageUploader extends StatelessWidget {
  const ImageUploader(
      {Key? key, required this.onImageUploaded, required this.path})
      : super(key: key);

  /// Ruta de la imagen, sin tener en cuenta el nombre, por ejemplo, 'images/works'.
  final String path;

  /// Callback que se va a ejecutar cuando se haya subido la imagen.
  final Function(String imageUploadedURL) onImageUploaded;

  @override
  Widget build(BuildContext context) {
    bool isUploading = false;
    Widget? imageUploaded;
    double imageUploadProgress = 0;

    return StatefulBuilder(
        builder: (BuildContext context, StateSetter setState) {
      return Stack(alignment: Alignment.center, children: [
        SizedBox(
            width: double.infinity,
            height: double.infinity,
            child: imageUploaded),
        // Add photo button.
        Visibility(
          visible: imageUploaded == null,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              IconButton(
                  icon: const Icon(
                    Icons.add_a_photo_outlined,
                  ),
                  iconSize: 50.0,
                  onPressed: () async {
                    FilePickerResult? result = await FilePicker.platform
                        .pickFiles(type: FileType.image);
                    if (result != null) {
                      var imagePath = '$path/${result.files.first.name}';

                      // Upload image to firebase storage.
                      FirebaseService()
                          .uploadImage(result.files.first.bytes!, imagePath,
                              (TaskSnapshot event) {
                        setState(() {
                          isUploading = event.state == TaskState.running;
                          imageUploadProgress =
                              event.bytesTransferred / event.totalBytes;
                        });
                      }).then((either) {
                        either.fold((imageURL) {
                          setState(() {
                            imageUploaded =
                                Image.network(imageURL, fit: BoxFit.cover);
                            onImageUploaded(imageURL);
                          });
                        }, (e) {
                          setState(() {
                            // Handle image upload error.
                            imageUploaded = null;
                            onImageUploaded('');
                          });
                        });
                      });
                    }
                  }),
              const Text('Subir Imagen')
            ],
          ),
        ),
        // Image upload progress bar.
        Visibility(
          visible: isUploading,
          child: LinearProgressIndicator(
            value: imageUploadProgress,
            semanticsValue: '$imageUploadProgress%',
          ),
        )
      ]);
    });
  }
}
