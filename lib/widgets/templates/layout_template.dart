import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:print_ideas_3d/constants/app_colors.dart';
import 'package:print_ideas_3d/constants/constants.dart';
import 'package:print_ideas_3d/models/core/app_state_model.dart';
import 'package:print_ideas_3d/models/core/drawer_state_model.dart';
import 'package:print_ideas_3d/widgets/navigation_bar/drawer.dart';
import 'package:print_ideas_3d/widgets/navigation_bar/navigation_bar.dart';
import 'package:provider/provider.dart';
import 'package:responsive_builder/responsive_builder.dart';

import '../../router/ui_pages.dart';

/// Layout principal de la app. Aqui se define la estructura de la app.
class LayoutTemplate extends StatefulWidget {
  const LayoutTemplate({Key? key, required this.child}) : super(key: key);

  final Widget child;

  @override
  _LayoutTemplateState createState() => _LayoutTemplateState();
}

class _LayoutTemplateState extends State<LayoutTemplate> {
  // Objects for the Router
  ChildBackButtonDispatcher? _backButtonDispatcher;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    // Defer back button dispatching to the child router
    _backButtonDispatcher = Router.of(context)
        .backButtonDispatcher!
        .createChildBackButtonDispatcher();
  }

  @override
  Widget build(BuildContext context) {
    // Claim priority, If there are parallel sub router, you will need
    // to pick which one should take priority;
    _backButtonDispatcher!.takePriority();

    return ResponsiveBuilder(
      builder: (context, sizingInfo) {
        final bodyHPadding = sizingInfo.isMobile ? 0.0 : kBodyHPadding;
        AppStateModel appState = Provider.of<AppStateModel>(context);
        DrawerStateModel drawerState = Provider.of<DrawerStateModel>(context);
        var selectedBottomIndex = bottomNavBarItems.keys
            .toList(growable: false)
            .indexOf(appState.currentPage);

        return Scaffold(
          // Header: Barra de navegación responsiva a través del appBar.
          appBar: sizingInfo.isMobile
              ? ResponsiveNavigationBar.appBarMobile(context)
              : ResponsiveNavigationBar.appBarTabletDesktop(
                  context, kDesktopNavbarHeight),
          // Drawer. Solo en pantallas chicas.
          drawer: sizingInfo.isMobile ? const AppDrawer() : null,
          onDrawerChanged: (isOpen) {
            drawerState.isDrawerOpened = isOpen;
          },
          // Aca se muestra el contenido.
          body: Container(
            padding: EdgeInsets.symmetric(
                horizontal: bodyHPadding, vertical: bodyHPadding),
            constraints: const BoxConstraints(minHeight: 500.0),
            child: widget.child,
          ),
          // Barra de navegación inferior para móviles.
          bottomNavigationBar: sizingInfo.isMobile &&
                  bottomNavBarItems.containsKey(appState.currentPage)
              ? BottomNavigationBar(
                  selectedItemColor: kSecondaryColor,
                  unselectedItemColor: kPrimaryColor,
                  items: bottomNavBarItems.values.toList(growable: false),
                  currentIndex:
                      selectedBottomIndex >= 0 ? selectedBottomIndex : 0,
                  onTap: (index) {
                    Pages page = bottomNavBarItems.keys
                        .toList(growable: false)
                        .elementAt(index);

                    context.goNamed(page.name);
                    appState.currentPage = page;
                  },
                  showSelectedLabels: true,
                  showUnselectedLabels: true,
                )
              : null,
        );
      },
    );
  }
}
