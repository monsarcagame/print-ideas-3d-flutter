import 'package:flutter/material.dart';
import 'package:print_ideas_3d/constants/text_styles.dart';
import 'package:print_ideas_3d/models/core/app_state_model.dart';
import 'package:print_ideas_3d/services/firebase_service.dart';
import 'package:print_ideas_3d/router/ui_pages.dart';
import 'package:print_ideas_3d/widgets/dialogs/dialogs.dart';
import 'package:provider/provider.dart';

/// Barra de autenticación.
class AuthBar extends StatefulWidget {
  const AuthBar({
    Key? key,
  }) : super(key: key);

  @override
  _AuthBarState createState() => _AuthBarState();
}

class _AuthBarState extends State<AuthBar> {
  @override
  Widget build(BuildContext context) {
    return Consumer<AppStateModel>(builder: (context, appState, child) {
      // Si no hay un usuario logueado.
      if (!appState.printUser.isSignedIn) {
        return Container(
          padding: const EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
          child: ElevatedButton(
              onPressed: () {
                appState.currentPage = Pages.login;
              },
              child: const Text('Iniciar sesión',
                  style: TextStyle(fontSize: kTextSizeAuth))),
        );
      } else {
        // Barra de perfil de usuario.
        return Padding(
          padding: const EdgeInsets.only(right: 8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    appState.printUser.email,
                    style: const TextStyle(color: kTextColorMainTheme),
                  ),
                  Visibility(
                      visible: appState.printUser.isAdmin,
                      child: const Text('(Admin)',
                          style: TextStyle(color: kTextColorCardDefault))),
                  // Log out.
                  IconButton(
                      onPressed: () {
                        // Key para poder cerrar el loading dialog.
                        GlobalKey<State> keyLoader = GlobalKey<State>();
                        // Mostrar dialogo de espera.
                        Dialogs.showLoadingDialog(context,
                            key: keyLoader, loadingMessage: 'Cerrando sesíon');
                        // Esperar por el cierre de sesion.
                        FirebaseService().signOutUser().then((value) {
                          if (keyLoader.currentContext != null) {
                            Navigator.of(keyLoader.currentContext!,
                                    rootNavigator: true)
                                .pop();
                          }
                          // Despues de cerrar sesíon, voy a home.
                          if (appState.currentPage != Pages.home) {
                            appState.currentPage = Pages.home;
                          }
                        });
                      },
                      icon: const Icon(Icons.logout)),
                ],
              ),
            ],
          ),
        );
      }
    });
  }
}
