import 'package:flutter/material.dart';
import 'package:print_ideas_3d/app_config.dart';
import 'package:responsive_builder/responsive_builder.dart';

import 'helpers/responsive_helper.dart';
import 'main.dart';

void main() async {
  // Set custom responsive breakpoints.
  ResponsiveSizingConfig.instance
      .setCustomBreakpoints(kCustomResponsiveBreakpoints);

  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp(AppConfig(buildFlavor: Flavor.production)));
}
