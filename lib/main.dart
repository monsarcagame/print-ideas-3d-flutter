import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:print_ideas_3d/constants/app_colors.dart';
import 'package:print_ideas_3d/models/core/app_state_model.dart';
import 'package:print_ideas_3d/models/core/business_model.dart';
import 'package:print_ideas_3d/models/core/drawer_state_model.dart';
import 'package:print_ideas_3d/router/router.dart';
import 'package:print_ideas_3d/views/error/initialization_failed.dart';
import 'package:print_ideas_3d/views/splash/splash_view.dart';
import 'package:provider/provider.dart';

import 'init.dart';
import 'dart:developer' as developer;

class MyApp extends StatelessWidget {
  // Create the initialization Future outside of 'build'.
  final Future<BusinessModel> _initialization;
  // Clases para el Router principal.

  // appCofig tiene la información necesaria para inicializar la app.
  MyApp(appConfig, {Key? key})
      : _initialization = Init.initialize(appConfig),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<BusinessModel>(
      future: _initialization,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          developer.log('Error: ${snapshot.error.toString()}');
          // Handle error.
          return const MaterialApp(
            debugShowCheckedModeBanner: false,
            home: InitializationError(),
          );
        } else if (snapshot.hasData) {
          final GlobalKey<NavigatorState> rootNavigatorKey =
              GlobalKey<NavigatorState>();
          // Providers definitions.
          BusinessModel business = snapshot.data!;
          AppStateModel appState = AppStateModel();

          return MultiProvider(
              providers: [
                // Provide BussinesModel
                Provider<BusinessModel>.value(value: business),
                // Provide AppStateModel
                ChangeNotifierProvider(create: (_) => appState),
                // Provider para notificar el estado del drawer.
                ChangeNotifierProvider<DrawerStateModel>(
                    create: (_) => DrawerStateModel())
              ],
              child: MaterialApp.router(
                debugShowCheckedModeBanner: false,
                title: 'Print.Ideas3D',
                routerConfig:
                    RouterHelper.goRouter(rootNavigatorKey: rootNavigatorKey),
                // Definir los temas de la app.
                theme: ThemeData(
                  primarySwatch: Colors.lightGreen,
                  brightness: Brightness.dark,
                  // App bar.
                  appBarTheme: const AppBarTheme(
                    foregroundColor: kTopNavigationBarForegroundColor,
                  ),
                  // Icons theme.
                  iconTheme:
                      const IconThemeData(color: kPrimaryColor, opacity: 1.0),
                  // Text theme.
                  textTheme:
                      GoogleFonts.ptSerifTextTheme(Typography.whiteHelsinki),
                  // Dialog theme.
                  dialogTheme: const DialogTheme(
                      titleTextStyle: TextStyle(
                          color: kPrimaryColor, fontWeight: FontWeight.bold),
                      contentTextStyle: TextStyle(color: Colors.white)),
                ),
              ));
        } else {
          // Mientras se espera que se cargue la configuración...
          return const MaterialApp(
            debugShowCheckedModeBanner: false,
            home: SplashView(),
          );
        }
      },
    );
  }
}
