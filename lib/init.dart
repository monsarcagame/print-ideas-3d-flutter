import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:print_ideas_3d/app_config.dart';
import 'package:print_ideas_3d/helpers/error_handling.dart';
import 'package:print_ideas_3d/models/core/business_model.dart';

import 'firebase_options.dart';
import 'dart:developer' as developer;

class Init {
  /// Dada una configuración, inicializa la app.
  static Future<BusinessModel> initialize(AppConfig config) async {
    // Primero Inicializar firebase para poder usar todos sus servicios.
    try {
      await Firebase.initializeApp(
        options: DefaultFirebaseOptions.currentPlatform,
      );
      developer.log('App initialized succesfull');
      // Development flavor.
      if (config.buildFlavor == Flavor.dev) {
        developer.log('Set firebase emulator');
        // Firebase Emulator Suite.
        try {
          FirebaseFirestore.instance.useFirestoreEmulator('localhost', 8080);
          FirebaseFunctions.instance.useFunctionsEmulator('localhost', 5001);
        } catch (e) {
          developer.log('firebase emulator error');
          final String code = (e as dynamic).code;
          if (code != "failed-precondition") {
            rethrow;
          }
        }

        await FirebaseAuth.instance.useAuthEmulator('localhost', 9099);
        await FirebaseStorage.instance.useStorageEmulator('localhost', 9199);
        // Delay to simulate loading letency.
        await Future.delayed(const Duration(seconds: 3));
      }
      // Cargar la configuración del negocio.
      developer.log('Loading settings');
      var businessSetting = await _loadSettings();
      developer.log('Settings loaded');

      return businessSetting;
    } on FirebaseException catch (e) {
      developer.log('error1');
      throw Failure(code: e.code, message: 'Error: ${e.message}');
    } on Failure {
      developer.log('error2');
      rethrow;
    } catch (e) {
      rethrow;
    }
  }

  static Future<BusinessModel> _loadSettings() async {
    final db = FirebaseFirestore.instance;

    try {
      var bussinesSettingSnapshot =
          await db.collection('appSettings').doc('businessSetting').get();
      var businessModel =
          BusinessModel.fromJSON(bussinesSettingSnapshot.data());

      return businessModel;
    } on FirebaseException catch (e) {
      throw Failure(
          code: e.code,
          message: 'Error al cargar configuraciones: ${e.message}');
    }
  }
}
