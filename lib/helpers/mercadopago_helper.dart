import '../models/shop/cart_model.dart';

class MercadoPagoHelper {
  static Map<String, dynamic> createPreferenceFromCart(Cart cart) {
    List<Map<String, dynamic>> items = [];
    // Items.
    items.addAll(cart.items
        .map((cartItem) => {
              'id': cartItem.id,
              'title': cartItem.title,
              'description': cartItem.description,
              'picture_url': cartItem.imageURL,
              'category_id':
                  'others', // se refiere a las categorias definidas por mercadopago.
              'quantity': cart.getItemCount(cartItem),
              'currency_id': 'ARS',
              'unit_price': cartItem.price
            })
        .toList());
    // Payer. Se supone que el payer es el shippment receiver.
    CompleteAddress? shippingAddress = cart.shippingInfo.destination;
    Map<String, dynamic> payer = {};
    Map<String, dynamic> shipments = {};
    if (shippingAddress != null) {
      payer = {
        'name': shippingAddress.contact,
        'surname': '',
        'email': '',
        'phone': {'area_code': '', 'number': ''},
      };
      // Shipments.
      shipments = {
        'mode': 'not_specified',
        'cost': cart.shippingCost,
        'expires': true,
        'receiver_address': {
          'zip_code': shippingAddress.zipCode,
          'street_name': shippingAddress.street,
          'street_number': shippingAddress.number,
          'city_name': shippingAddress.city,
          'state_name': shippingAddress.state,
          'floor': shippingAddress.floor,
          'apartment': shippingAddress.apartment
        }
      };
    }

    return {
      'items': items,
      'payer': payer,
      'shipments': shipments
      //'back_urls': {}
    };
  }
}
