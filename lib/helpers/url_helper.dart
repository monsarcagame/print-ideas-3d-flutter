import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/foundation.dart'
    show TargetPlatform, defaultTargetPlatform;

class UrlHelper {
  /// Launch a valid URL.
  static void launchURL(String? url) async {
    if (url != null) {
      await canLaunchUrl(Uri.parse(url))
          ? await launchUrl(Uri.parse(url))
          : throw 'No se puede abrir la url: $url';
    } else {
      throw 'No se puede abrir la url: $url';
    }
  }

  /// Open whatsapp.
  static Future<void> openwhatsapp(
      BuildContext context, String whatsapp, String message) async {
    var whatsappUrlIOS = "https://wa.me/$whatsapp/?text=${Uri.parse(message)}";
    var whatsappUrlAndroid =
        "https://api.whatsapp.com/send?phone=$whatsapp&text=${Uri.parse(message)}";

    if (defaultTargetPlatform == TargetPlatform.iOS) {
      // for iOS phone only
      if (await canLaunchUrl(Uri.parse(whatsappUrlIOS))) {
        await launchUrl(Uri.parse(whatsappUrlIOS));
      } else {
        if (context.mounted) {
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text("whatsapp no installed"),
              backgroundColor: Colors.red,
            ),
          );
        }
      }
    } else {
      // android , web
      if (await canLaunchUrl(Uri.parse(whatsappUrlAndroid))) {
        await launchUrl(Uri.parse(whatsappUrlAndroid));
      } else {
        if (context.mounted) {
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text("whatsapp no installed"),
              backgroundColor: Colors.red,
            ),
          );
        }
      }
    }
  }
}
