import 'dart:math';

import 'package:responsive_builder/responsive_builder.dart';

/// Custom responsive breakpoints.
const ScreenBreakpoints kCustomResponsiveBreakpoints =
    ScreenBreakpoints(desktop: 950, tablet: 768, watch: 300);

/// Ancho de pantalla base para el diseño responsivo.
const kDesignWidth = 768.0; // iPad: 768 x 1024
const kMinTextMultiplier = 0.6;
const kMaxTextMultiplier = 1.2;

/// Helper de ayuda para diseño responsivo.
class ResponsiveHelper {
  static int gridCrossAxisCount(SizingInformation sizingInfo) {
    return sizingInfo.screenSize.width <= 1024 ? 2 : 3;
  }

  static double textSizeMultiplier(SizingInformation sizingInfo) {
    return min(
      max(sizingInfo.screenSize.width / kDesignWidth, kMinTextMultiplier),
      kMaxTextMultiplier,
    );
  }
}
