import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:print_ideas_3d/helpers/error_handling.dart';
import 'package:print_ideas_3d/models/shop/cart_model.dart';

/// Lista de estados para shipping.
const List<String> shippingStates = [
  'Buenos Aires',
  'Catamarca',
  'Chaco',
  'Chubut',
  'Córdoba'
      'Corrientes',
  'Entre Ríos',
  'Formosa',
  'Jujuy',
  'La Pampa',
  'La Rioja',
  'Mendoza',
  'Misiones',
  'Neuquén',
  'Río Negro',
  'Salta',
  'San Juan',
  'San Luis',
  'Santa Cruz',
  'Santa Fe',
  'Sgo. del Estero',
  'Tierra del Fuego',
  'Tucumán'
];

/// Helper con funciones que permiten manejar la gestión de envíos.
/// Los cálculos estan basados en paq.ar.
class PaqArShippingHelper {
  /// Dado un origen y destino, obtener la zona correspondiente.
  static Future<int> getZone(Address? origin, Address? destination) async {
    if (origin != null && destination != null) {
      if (origin.zipCode == destination.zipCode) {
        return Future.value(1);
      } else {
        try {
          DocumentSnapshot<Map<String, dynamic>> docSnapshot =
              await FirebaseFirestore.instance
                  .collection('paqArZones')
                  .doc(origin.state)
                  .get();
          return docSnapshot.data()![destination.state];
        } on FirebaseException {
          return Future.value(-1);
        }
      }
    }
    return Future.value(-1);
  }

  /// Calcular el precio de enviar el carrito de compras a un destino.
  /// El cálculo se basa en una logica de zonas, donde a cada zona y peso
  /// le corresponde un precio. Otra variable que afecta el precio es si el envíos es a domicilio
  /// o retiro en sucursal. Si el modo de envío es LocalPickup, el costo es $0.0.
  static Future<double> getShippingPrice(
      ShippingInformation shippingInfo) async {
    if (shippingInfo.shippingMode == ShippingMode.localPickup) {
      return Future.value(0.0);
    }
    // Para hacer el cálculo del envío debe haber un destino.
    else if (shippingInfo.destination != null) {
      int zone = await getZone(shippingInfo.origin, shippingInfo.destination);
      double weight = 0.5;

      try {
        CollectionReference colRef =
            FirebaseFirestore.instance.collection('paqArPrices');
        DocumentSnapshot<Map<String, dynamic>> docSnapshot;
        if (shippingInfo.shippingMode == ShippingMode.homeDelivery) {
          docSnapshot = await colRef.doc('domicilio').get()
              as DocumentSnapshot<Map<String, dynamic>>;
        } else {
          docSnapshot = await colRef.doc('sucursal').get()
              as DocumentSnapshot<Map<String, dynamic>>;
        }

        return docSnapshot.data()!.containsKey('zone$zone')
            ? docSnapshot.data()!['zone$zone']['$weight']
            : 0.0;
      } on FirebaseException catch (e) {
        throw Failure(
          code: e.code,
          message: e.message ?? 'Hubo un error al leer de la base de datos.',
        );
      }
    }

    return 0.0;
  }

  /// Inizializar la colección 'paqArZones' en firestore. Esta función deberia llamarla solo en desarrollo.
  static Future<void> initializePaqArZones() {
    CollectionReference zones =
        FirebaseFirestore.instance.collection('paqArZones');

    return Future.forEach<String>(shippingStates, (state) {
      Map<String, int> map = {};
      for (var item in shippingStates) {
        map[item] = 1;
      }
      zones.doc(state).set(map);
    });
  }
}
