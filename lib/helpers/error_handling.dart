import 'package:firebase_auth/firebase_auth.dart';

class Failure {
  final String code;
  final String message;

  Failure({required this.code, required this.message});

  @override
  String toString() {
    return message;
  }
}

class SignInFailure extends Failure {
  final String email;
  final AuthCredential credential;

  SignInFailure(
      {required String code,
      required String message,
      required this.email,
      required this.credential})
      : super(message: message, code: code);
}
