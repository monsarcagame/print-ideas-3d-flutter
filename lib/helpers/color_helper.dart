import 'package:flutter/material.dart';

class ColorHelper {
  /// Representación de un Color en hexadecimal, sin alpha channel.
  static String getHexStringFromColor(Color? color) {
    // Color(0xAARRGGBB), Alpha=AA, Red=RR, Green=GG, Blue=BB hexadecimal
    return color.toString().substring(10, 16);
  }

  /// Get color from string. El string es de la forma "RRGGBB".
  static Color getColorFromString(String strColor) {
    return Color(int.parse(strColor, radix: 16) + 0xFF000000);
  }
}
