import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:print_ideas_3d/constants/app_colors.dart';
import 'package:print_ideas_3d/helpers/error_handling.dart';
import 'package:print_ideas_3d/models/work/printing_work_model.dart';
import 'package:print_ideas_3d/widgets/dialogs/dialogs.dart';
import 'package:transparent_image/transparent_image.dart';

class WorkDetailView extends StatelessWidget {
  const WorkDetailView({Key? key, required this.printingWorkId})
      : super(key: key);

  final String printingWorkId;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<PrintingWorkModel>(
      future: getPrintingWork(printingWorkId),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(
            child: LinearProgressIndicator(),
          );
        } else if (snapshot.hasError) {
          Dialogs.showMessageDialog(
            context,
            message: Text(
              snapshot.error!.toString(),
            ),
            alertType: AlertType.error,
          );
        } else if (snapshot.hasData) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              // Printing work image
              Expanded(
                child: ClipRRect(
                  child: FittedBox(
                    fit: BoxFit.cover,
                    child: FadeInImage.memoryNetwork(
                      placeholder: kTransparentImage,
                      image: snapshot.data!.imageURL,
                      imageErrorBuilder: (context, exception, stackTrace) {
                        return Image.asset('assets/images/missing_image.png');
                      },
                    ),
                  ),
                ),
              ),
              // Printing work detail
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // Title
                      Text(
                        snapshot.data!.title,
                        style: TextStyle(
                          fontSize:
                              Theme.of(context).textTheme.titleLarge?.fontSize,
                          color: kPrimaryColor,
                        ),
                      ),
                      const SizedBox(
                        height: 8.0,
                      ),
                      // Material
                      RichText(
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: 'Trabajo realizado con material ',
                              style: TextStyle(
                                fontSize: Theme.of(context)
                                    .textTheme
                                    .bodyLarge
                                    ?.fontSize,
                                color: Colors.white,
                              ),
                            ),
                            // Material
                            TextSpan(
                              text: snapshot.data!.material,
                              style: TextStyle(
                                fontSize: Theme.of(context)
                                    .textTheme
                                    .bodyLarge
                                    ?.fontSize,
                                color: kSecondaryColor,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 8.0,
                      ),
                      // Description
                      Text(
                        snapshot.data!.description,
                        style: TextStyle(
                          fontSize:
                              Theme.of(context).textTheme.bodyMedium?.fontSize,
                          color: Colors.white70,
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          );
        }

        return const Text('No data!');
      },
    );
  }
}

/// Get printing work from database.
Future<PrintingWorkModel> getPrintingWork(String id) async {
  try {
    DocumentSnapshot workDocSnapshot = await FirebaseFirestore.instance
        .collection('printingWorks')
        .doc(id)
        .get();
    Map<String, dynamic> data = workDocSnapshot.data() as Map<String, dynamic>;

    return PrintingWorkModel.fromJSON(id: id, data: data);
  } on FirebaseException catch (e) {
    throw Failure(code: e.code, message: e.message ?? 'Hubo un error!');
  }
}
