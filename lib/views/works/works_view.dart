import 'package:auto_size_text_pk/auto_size_text_pk.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:print_ideas_3d/constants/text_styles.dart';
import 'package:print_ideas_3d/helpers/error_handling.dart';
import 'package:print_ideas_3d/models/core/app_state_model.dart';
import 'package:print_ideas_3d/models/work/printing_work_model.dart';
import 'package:print_ideas_3d/router/ui_pages.dart';
import 'package:print_ideas_3d/widgets/dialogs/dialogs.dart';
import 'package:print_ideas_3d/widgets/image_uploader.dart';
import 'package:provider/provider.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:select_form_field/select_form_field.dart';
import 'package:transparent_image/transparent_image.dart';

class WorksView extends StatelessWidget {
  const WorksView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Get app state.
    var appState = Provider.of<AppStateModel>(context, listen: false);

    if (appState.printUser.isSignedIn && appState.printUser.isAdmin) {
      // ADMIN.
      return Scaffold(
          body: const PrintingWorksList(),
          floatingActionButton: FloatingActionButton(
              child: const Icon(Icons.add),
              onPressed: () async {
                // Agregar trabajos de impresión con un dialog.
                await showAddPrintingWorkDialog(context);
              }));
    } else {
      return const Scaffold(body: PrintingWorksList());
    }
  }
}

/// Responsive list of works.
class PrintingWorksList extends StatelessWidget {
  const PrintingWorksList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
        stream: getWorksStreamByDate(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          } else if (snapshot.hasError) {
            return const Text('Error!');
          } else if (snapshot.hasData) {
            return _buildResponsivePrintingWorkList(snapshot.data!.docs
                .map((docSnapshot) => PrintingWorkModel.fromJSON(
                    id: docSnapshot.id,
                    data: docSnapshot.data() as Map<String, dynamic>))
                .toList());
          }
          return const Text('no data');
        });
  }

  // Build responsive list.
  Widget _buildResponsivePrintingWorkList(List<PrintingWorkModel> works) {
    return ResponsiveBuilder(builder: (context, sizingInfo) {
      // Averiguar cuantas columnas tendrá la lista dependiendo del tamaño de pantalla.
      var gridCrossAxisCount = sizingInfo.isMobile ? 2 : 4;

      return GridView.count(
          childAspectRatio: 0.7,
          mainAxisSpacing: 2.0,
          crossAxisSpacing: 2.0,
          crossAxisCount: gridCrossAxisCount,
          children: works.map((workModel) {
            return GridTile(child: PrintingWorkCard(workModel: workModel));
          }).toList());
    });
  }
}

/// PrintingWork card.
class PrintingWorkCard extends StatelessWidget {
  final PrintingWorkModel workModel;
  const PrintingWorkCard({Key? key, required this.workModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (context, sizingInfo) {
      return InkWell(
        onTap: () {
          context.goNamed(
            Pages.workDetail.name,
            pathParameters: {'id': workModel.id!},
          );
        },
        child: Card(
          child: Column(
            children: [
              // Image.
              Expanded(
                flex: 2,
                child: SizedBox(
                  width: double.infinity,
                  height: double.infinity,
                  child: ClipRRect(
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(4.0),
                        topRight: Radius.circular(4.0)),
                    child: FittedBox(
                      fit: BoxFit.cover,
                      clipBehavior: Clip.hardEdge,
                      child: FadeInImage.memoryNetwork(
                        image: workModel.imageURL,
                        placeholder: kTransparentImage,
                        imageErrorBuilder: (context, exception, stackTrace) {
                          return Image.asset('assets/images/missing_image.png');
                        },
                      ),
                    ),
                  ),
                ),
              ),
              // Work info
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    ListTile(
                      dense: true,
                      contentPadding:
                          const EdgeInsets.symmetric(horizontal: 4.0),
                      // Title.
                      title: Text(
                        workModel.title,
                        style: const TextStyle(color: kTextColorCardTitle),
                      ),
                      // Material de impresión utilizado.
                      subtitle: Row(
                        children: [
                          const Text(
                            'Material: ',
                            style: TextStyle(
                              color: kTextColorCardDefault,
                            ),
                          ),
                          Text(
                            workModel.material,
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              color: kTextColorCardMaterial,
                            ),
                          )
                        ],
                      ),
                    ),
                    // Short Info.
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 4.0),
                        child: AutoSizeText(
                          workModel.shortInfo,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: Theme.of(context)
                                .textTheme
                                .bodyMedium
                                ?.fontSize,
                            color: kTextColorCardDefault,
                          ),
                          maxLines: 3,
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      );
    });
  }
}

/// Obtener la lista de trabajos de impresión de Firestore a través de un Stream.
Stream<QuerySnapshot> getWorksStreamByDate() {
  return FirebaseFirestore.instance.collection('printingWorks').snapshots();
}

/// Agregar un trabajo de impresión a la base de datos.
Future<void> addPrintingWork(PrintingWorkModel work) async {
  CollectionReference worksColRef =
      FirebaseFirestore.instance.collection('printingWorks');

  try {
    await worksColRef.add({
      'title': work.title,
      'shortInfo': work.shortInfo,
      'description': work.description,
      'imageURL': work.imageURL,
      'printingWorkDate': work.printingWorkDate,
      'material': work.material
    });
  } on FirebaseException catch (e) {
    throw Failure(code: e.code, message: '${e.message}');
  }
}

/// Formulario para agregar un trabajo de impresión por medio de un dialogo.
Future<void> showAddPrintingWorkDialog(BuildContext context) async {
  return await showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        // Key for validate form inputs.
        final GlobalKey<FormState> formKey = GlobalKey<FormState>();
        // Controllers.
        final TextEditingController titleTextEditingCtrl =
            TextEditingController();
        final TextEditingController shortInfoTextEditingCtrl =
            TextEditingController();
        final TextEditingController descriptionTextEditingCtrl =
            TextEditingController();
        DateTime? workDateTime = DateTime.now();
        final TextEditingController materialTextEditingCtrl =
            TextEditingController();
        String workImageURL = '';

        return AlertDialog(
          scrollable: true,
          title: const Text('Agregar trabajo de impresión'),
          content: Form(
              key: formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  // Work image uploader.
                  Container(
                    width: 100.0,
                    height: 100.0,
                    decoration: BoxDecoration(
                        border: Border.all(width: 2.0),
                        borderRadius: BorderRadius.circular(8.0)),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: ImageUploader(
                          path: 'images/works',
                          onImageUploaded: (imageUploadedURL) {
                            workImageURL = imageUploadedURL;
                          }),
                    ),
                  ),
                  // Title input.
                  TextFormField(
                    controller: titleTextEditingCtrl,
                    decoration:
                        const InputDecoration(hintText: 'Ingresa un título'),
                    validator: (value) => (value != null)
                        ? (value.isNotEmpty ? null : 'Campo requerido')
                        : null,
                  ),
                  // Short description input.
                  TextFormField(
                    controller: shortInfoTextEditingCtrl,
                    decoration: const InputDecoration(
                        hintText: 'Ingresa una breve descripción'),
                    validator: (value) => (value != null)
                        ? (value.isNotEmpty ? null : 'Campo requerido')
                        : null,
                  ),
                  // Description input.
                  TextFormField(
                    controller: descriptionTextEditingCtrl,
                    decoration: const InputDecoration(
                        hintText: 'Ingresa una descripción'),
                    validator: (value) => (value != null)
                        ? (value.isNotEmpty ? null : 'Campo requerido')
                        : null,
                  ),
                  // Fecha del trabajo de impresión.
                  StatefulBuilder(builder: (context, setState) {
                    return Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        ElevatedButton(
                            onPressed: () async {
                              final newDateTime = await showDatePicker(
                                  context: context,
                                  initialDate: DateTime.now(),
                                  firstDate: DateTime(DateTime.now().year - 5),
                                  lastDate: DateTime(DateTime.now().year + 5));
                              if (newDateTime != null) {
                                setState(() {
                                  workDateTime = newDateTime;
                                });
                              }
                            },
                            child: const Text('Fecha de creación')),
                        Text(workDateTime.toString())
                      ],
                    );
                  }),
                  // Material de impresíon.
                  SelectFormField(
                    controller: materialTextEditingCtrl,
                    decoration: const InputDecoration(
                        hintText: 'Ingresa el material con el que se imprimió'),
                    items: const [
                      {'value': 'PLA', 'label': 'PLA'},
                      {'value': 'PETG', 'label': 'PETG'}
                    ],
                    validator: (value) => (value != null)
                        ? (value.isNotEmpty ? null : 'Campo requerido')
                        : null,
                  )
                ],
              )),
          actions: [
            TextButton(
                onPressed: () async {
                  // Si el formulario es válido
                  if ((formKey.currentState != null
                          ? formKey.currentState!.validate()
                          : false) &&
                      workDateTime != null) {
                    // Intento agregar el trabajo de impresión en la base de datos.
                    try {
                      await addPrintingWork(PrintingWorkModel(
                          title: titleTextEditingCtrl.value.text,
                          shortInfo: shortInfoTextEditingCtrl.value.text,
                          description: descriptionTextEditingCtrl.value.text,
                          imageURL: workImageURL,
                          printingWorkDate: Timestamp.fromDate(
                              workDateTime ?? DateTime.now()),
                          material: materialTextEditingCtrl.value.text));
                      await Dialogs.showMessageDialog(context,
                          message:
                              const Text('Trabajo agregado correctamente'));
                      Navigator.of(context).pop();
                    } on Failure catch (failure) {
                      Dialogs.showMessageDialog(context,
                          message: Text('Error: ${failure.toString()}'),
                          alertType: AlertType.error);
                    }
                  }
                },
                child: const Text('Agregar'))
          ],
        );
      });
}
