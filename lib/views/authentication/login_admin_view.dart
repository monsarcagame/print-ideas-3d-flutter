import 'package:flutter/material.dart';
import 'package:print_ideas_3d/constants/constants.dart';
import 'package:print_ideas_3d/constants/text_styles.dart';
import 'package:print_ideas_3d/helpers/error_handling.dart';
import 'package:print_ideas_3d/helpers/form_validation_helper.dart';
import 'package:print_ideas_3d/models/core/app_state_model.dart';
import 'package:print_ideas_3d/services/firebase_service.dart';
import 'package:print_ideas_3d/router/ui_pages.dart';
import 'package:print_ideas_3d/widgets/dialogs/dialogs.dart';
import 'package:provider/provider.dart';
import 'package:responsive_builder/responsive_builder.dart';

class LoginAdminView extends StatefulWidget {
  const LoginAdminView({Key? key}) : super(key: key);

  @override
  _LoginAdminViewState createState() => _LoginAdminViewState();
}

class _LoginAdminViewState extends State<LoginAdminView> {
  String email = '', password = '';
  final _formKey = GlobalKey<FormState>();

  // Chekeo de campos.
  bool _checkFields() {
    final form = _formKey.currentState!;
    if (form.validate()) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    var appState = Provider.of<AppStateModel>(context);

    return ResponsiveBuilder(builder: (context, sizingInfo) {
      // Compute form width.
      var formWidth = sizingInfo.isMobile
          ? double.infinity
          : sizingInfo.isTablet
              ? sizingInfo.screenSize.width * 0.9
              : sizingInfo.screenSize.width * 0.5;

      return Center(
        child: SizedBox(
          width: formWidth,
          child: Column(
            children: [
              // Agrego un título para móbiles.
              Visibility(
                visible: !sizingInfo.isMobile,
                child: Text(
                  'Iniciar sesión',
                  style: TextStyle(
                    fontSize: Theme.of(context).textTheme.titleLarge?.fontSize,
                  ),
                ),
              ),
              // Form para el logueo del administrador.
              Column(
                children: [
                  Form(
                    key: _formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        // Email input.
                        Padding(
                          padding: kFormFieldPadding,
                          child: TextFormField(
                            style: kTextFormFieldStyle,
                            decoration: InputDecoration(
                                icon: Icon(
                                  Icons.email,
                                  color: IconTheme.of(context).color,
                                ),
                                hintText: 'Email'),
                            validator: (value) => value!.isEmpty
                                ? 'Campo requerido'
                                : FormValidationHelper.validateEmail(
                                    value.trim()),
                            onChanged: (value) {
                              email = value;
                            },
                          ),
                        ),
                        // Password input.
                        Padding(
                          padding: kFormFieldPadding,
                          child: TextFormField(
                            style: kTextFormFieldStyle,
                            obscureText: true,
                            decoration: InputDecoration(
                                icon: Icon(Icons.lock,
                                    color: IconTheme.of(context).color),
                                hintText: 'Password'),
                            validator: (value) =>
                                value!.isEmpty ? 'Campo requerido' : null,
                            onChanged: (value) {
                              password = value;
                            },
                          ),
                        ),
                        // Spacing.
                        const SizedBox(height: 16.0),
                        // Login button.
                        ElevatedButton(
                          child: const SizedBox(
                            height: 40.0,
                            width: 100,
                            child: Center(
                              child: Text('Iniciar sesión'),
                            ),
                          ),
                          onPressed: () async {
                            if (_checkFields()) {
                              try {
                                var user = await FirebaseService()
                                    .signInUser(email, password);
                                if (context.mounted) {
                                  Dialogs.showMessageDialog(
                                    context,
                                    message: Text('Bienvenido ${user?.email}'),
                                    alertType: AlertType.success,
                                    action: () =>
                                        appState.currentPage = Pages.home,
                                  );
                                }
                              } on Failure catch (f) {
                                Dialogs.showMessageDialog(
                                  context,
                                  message: Text(f.message),
                                  alertType: AlertType.error,
                                );
                              }
                            }
                          },
                        )
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      );
    });
  }
}
