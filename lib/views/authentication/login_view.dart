import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:print_ideas_3d/constants/constants.dart';
import 'package:print_ideas_3d/constants/text_styles.dart';
import 'package:print_ideas_3d/helpers/error_handling.dart';
import 'package:print_ideas_3d/helpers/form_validation_helper.dart';
import 'package:print_ideas_3d/models/core/app_state_model.dart';
import 'package:print_ideas_3d/services/firebase_service.dart';
import 'package:print_ideas_3d/router/ui_pages.dart';
import 'package:print_ideas_3d/widgets/dialogs/dialogs.dart';
import 'package:provider/provider.dart';
import 'package:responsive_builder/responsive_builder.dart';

class LoginView extends StatefulWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  String email = '', password = '';
  final _formKey = GlobalKey<FormState>();

  bool _checkFields() {
    final form = _formKey.currentState!;
    if (form.validate()) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    var appState = Provider.of<AppStateModel>(context);

    return ResponsiveBuilder(builder: (context, sizingInfo) {
      // Compute form width.
      var formWidth = sizingInfo.isMobile
          ? double.infinity
          : sizingInfo.isTablet
              ? sizingInfo.screenSize.width * 0.9
              : sizingInfo.screenSize.width * 0.5;

      return Center(
        child: SizedBox(
          width: formWidth,
          child: Column(
            children: [
              Visibility(
                visible: !sizingInfo.isMobile,
                child: Text(
                  'Iniciar sesión',
                  style: TextStyle(
                    fontSize: Theme.of(context).textTheme.titleLarge?.fontSize,
                  ),
                ),
              ),
              // Form.
              Column(
                children: [
                  Form(
                      key: _formKey,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          // Email input.
                          Padding(
                            padding: kFormFieldPadding,
                            child: TextFormField(
                              style: kTextFormFieldStyle,
                              decoration: InputDecoration(
                                  icon: Icon(
                                    Icons.email,
                                    color: IconTheme.of(context).color,
                                  ),
                                  hintText: 'Email'),
                              validator: (value) => value!.isEmpty
                                  ? 'Campo requerido'
                                  : FormValidationHelper.validateEmail(
                                      value.trim()),
                              onChanged: (value) {
                                email = value;
                              },
                            ),
                          ),
                          // Password input.
                          Padding(
                            padding: kFormFieldPadding,
                            child: TextFormField(
                              style: kTextFormFieldStyle,
                              obscureText: true,
                              decoration: InputDecoration(
                                  icon: Icon(Icons.lock,
                                      color: IconTheme.of(context).color),
                                  hintText: 'Password'),
                              validator: (value) =>
                                  value!.isEmpty ? 'Campo requerido' : null,
                              onChanged: (value) {
                                password = value;
                              },
                            ),
                          ),
                          // Spacing.
                          const SizedBox(height: 16.0),
                          // Login button.
                          ElevatedButton(
                            child: const SizedBox(
                              height: 40.0,
                              width: 100,
                              child: Center(
                                child: Text('Iniciar sesión'),
                              ),
                            ),
                            onPressed: () async {
                              if (_checkFields()) {
                                try {
                                  var user = await FirebaseService()
                                      .signInUser(email, password);
                                  if (context.mounted) {
                                    Dialogs.showMessageDialog(context,
                                        message:
                                            Text('Bienvenido ${user?.email}'),
                                        alertType: AlertType.success);
                                  }
                                } on Failure catch (f) {
                                  Dialogs.showMessageDialog(
                                    context,
                                    message: Text(f.message),
                                    alertType: AlertType.error,
                                  );
                                }
                              }
                            },
                          )
                        ],
                      ))
                ],
              ),
              const SizedBox(height: 16.0),
              _socialSignIn(appState),
              const SizedBox(height: 16.0),
              RichText(
                  text: TextSpan(children: [
                const TextSpan(
                    text: 'No tienes una Cuenta? ',
                    style: TextStyle(color: Colors.white)),
                TextSpan(
                    text: 'Registrarse',
                    style: const TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        appState.currentPage = Pages.signIn;
                      })
              ]))
            ],
          ),
        ),
      );
    });
  }

  /// Social Authentication.
  Widget _socialSignIn(AppStateModel appState) {
    return Column(
      children: [
        const Text(
          '- Ó -',
          style: TextStyle(color: Colors.white),
        ),
        const SizedBox(height: 8.0),
        const Text('Iniciar sesión con', style: TextStyle(color: Colors.white)),
        const SizedBox(height: 8.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            // Google.
            GestureDetector(
              onTap: () async {
                try {
                  var userCredential =
                      await FirebaseService().signInWithGoogle();
                  if (context.mounted) {
                    Dialogs.showMessageDialog(
                      context,
                      message: Text(
                        'Bienvenido ${userCredential.user?.displayName ?? userCredential.user?.email}',
                      ),
                      alertType: AlertType.success,
                    );
                  }
                  appState.currentPage = Pages.home;
                } on SignInFailure catch (f) {
                  await _handleSignInFailure(context, f, appState);
                }
              },
              child: Container(
                height: 60.0,
                width: 60.0,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black26,
                        offset: Offset(0, 2.0),
                        blurRadius: 6.0)
                  ],
                  image: DecorationImage(
                    image: AssetImage('assets/images/icons/icon_google.png'),
                  ),
                ),
              ),
            ),
            // Facebook.
            GestureDetector(
              onTap: () async {
                try {
                  var userCredential =
                      await FirebaseService().signInWithFacebook();
                  if (context.mounted) {
                    Dialogs.showMessageDialog(
                      context,
                      message: Text(
                        'Bienvenido ${userCredential.user?.displayName ?? userCredential.user?.email}',
                      ),
                      alertType: AlertType.success,
                    );
                  }
                } on SignInFailure catch (f) {
                  await _handleSignInFailure(context, f, appState);
                }
              },
              child: Container(
                height: 60.0,
                width: 60.0,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black26,
                      offset: Offset(0, 2.0),
                      blurRadius: 6.0,
                    )
                  ],
                  image: DecorationImage(
                    image: AssetImage(
                      'assets/images/icons/icon_facebook_mobile.png',
                    ),
                  ),
                ),
              ),
            )
          ],
        )
      ],
    );
  }

  /// Gestionar los errores de logueo.
  Future<void> _handleSignInFailure(
      BuildContext context, SignInFailure f, AppStateModel appState) async {
    switch (f.code) {
      case 'account-exists-with-different-credential':
        List<String> userSignInMethods =
            await FirebaseAuth.instance.fetchSignInMethodsForEmail(f.email);

        AuthCredential pendingCredential = f.credential;

        // Password.
        if (userSignInMethods.first == 'password') {
          // Prompt the user to enter their password
          String password = await Dialogs.showPasswordInputDialog(context);

          // Sign the user in to their account with the password
          UserCredential userCredential =
              await FirebaseAuth.instance.signInWithEmailAndPassword(
            email: email,
            password: password,
          );

          // Link the pending credential with the existing account
          await userCredential.user!.linkWithCredential(pendingCredential);
        }
        // Google
        if (userSignInMethods.first == 'google.com') {
          UserCredential userCredential =
              await FirebaseService().signInWithGoogle();

          await userCredential.user!.linkWithCredential(pendingCredential);
        }
        break;
      default:
        Dialogs.showMessageDialog(
          context,
          message: Text(f.message),
          alertType: AlertType.error,
        );
    }
  }
}
