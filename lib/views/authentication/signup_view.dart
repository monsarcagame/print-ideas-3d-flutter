import 'package:flutter/material.dart';
import 'package:print_ideas_3d/constants/constants.dart';
import 'package:print_ideas_3d/constants/text_styles.dart';
import 'package:print_ideas_3d/helpers/error_handling.dart';
import 'package:print_ideas_3d/helpers/form_validation_helper.dart';
import 'package:print_ideas_3d/models/core/app_state_model.dart';
import 'package:print_ideas_3d/router/ui_pages.dart';
import 'package:print_ideas_3d/services/firebase_service.dart';
import 'package:print_ideas_3d/widgets/dialogs/dialogs.dart';
import 'package:provider/provider.dart';
import 'package:responsive_builder/responsive_builder.dart';

class SignInView extends StatefulWidget {
  const SignInView({Key? key}) : super(key: key);

  @override
  _SignInViewState createState() => _SignInViewState();
}

class _SignInViewState extends State<SignInView> {
  String? email;
  // Keys.
  final _loadingDialogKey = GlobalKey();
  final _formKey = GlobalKey<FormState>();
  // Controllers.
  final _emailController = TextEditingController();

  @override
  void dispose() {
    _emailController.dispose();
    super.dispose();
  }

  bool checkFields() {
    final form = _formKey.currentState!;
    if (form.validate()) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (context, sizingInfo) {
      // Compute form width.
      var formWidth = sizingInfo.isMobile
          ? double.infinity
          : sizingInfo.isTablet
              ? sizingInfo.screenSize.width * 0.9
              : sizingInfo.screenSize.width * 0.5;

      return Center(
        child: SizedBox(
          width: formWidth,
          child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // Email TextFormField.
                  Padding(
                    padding: kFormFieldPadding,
                    child: TextFormField(
                      controller: _emailController,
                      style: const TextStyle(color: kTextColorDefault),
                      decoration: InputDecoration(
                          icon: Icon(Icons.email,
                              color: IconTheme.of(context).color),
                          hintText: 'Email'),
                      validator: (value) => value!.isEmpty
                          ? 'Email is required'
                          : FormValidationHelper.validateEmail(value.trim()),
                    ),
                  ),
                  // Spacing.
                  const SizedBox(height: 16.0),
                  // Sign In button.
                  ElevatedButton(
                      child: Container(
                        height: 40.0,
                        width: 100,
                        decoration:
                            BoxDecoration(color: Colors.green.withOpacity(0.2)),
                        child: const Center(
                          child: Text('Sign in'),
                        ),
                      ),
                      onPressed: () async {
                        if (checkFields()) {
                          try {
                            // Mostrar un loading dialog.
                            Dialogs.showLoadingDialog(
                              context,
                              key: _loadingDialogKey,
                              loadingMessage: 'Por favor espere...',
                            );
                            // Intento agregar el nuevo usuario.
                            await FirebaseService().signInWithEmailAndLink(
                              _emailController.value.text,
                            );
                            // Cerrar el loading dialog.
                            if (_loadingDialogKey.currentContext != null) {
                              Navigator.of(_loadingDialogKey.currentContext!)
                                  .pop();
                            }
                            // Mostrar un dialog con información de la operación.
                            if (context.mounted) {
                              Dialogs.showMessageDialog(
                                context,
                                message: Text(
                                    'Se ha enviado un link a $email para que puedas iniciar sesión'),
                                action: () => Provider.of<AppStateModel>(
                                        context,
                                        listen: false)
                                    .currentPage = Pages.login,
                              );
                            }
                          } on Failure catch (f) {
                            Dialogs.showMessageDialog(
                              context,
                              message: Text(f.message),
                            );
                          }
                        }
                      })
                ],
              )),
        ),
      );
    });
  }
}
