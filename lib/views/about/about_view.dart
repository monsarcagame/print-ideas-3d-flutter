import 'package:flutter/material.dart';
import 'package:print_ideas_3d/constants/app_colors.dart';
import 'package:print_ideas_3d/helpers/responsive_helper.dart';
import 'package:print_ideas_3d/models/core/business_model.dart';
import 'package:provider/provider.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

const aboutBackgroundImage = "assets/images/print3d_logo_mockup.webp";
const aboutImageList = [
  "assets/images/about/image_about_land_01.jpg",
  "assets/images/about/image_about_port_01.jpg",
  "assets/images/about/image_about_port_02.jpg",
  "assets/images/about/image_about_port_03.jpg",
  "assets/images/about/image_about_land_02.jpg",
  "assets/images/about/image_about_port_04.jpg",
  "assets/images/about/image_about_port_05.jpg",
  "assets/images/about/image_about_port_06.jpg",
  "assets/images/about/image_about_land_03.jpg",
];

class AboutView extends StatelessWidget {
  const AboutView({Key? key}) : super(key: key);

  /// Dado un índice, genera un tile para el collage.
  Widget _collageTile(int index) {
    if (index < aboutImageList.length) {
      return Image.asset(aboutImageList[index]);
    } else {
      return Container();
    }
  }

  /// Genera el collage de fotos.
  Widget _buildCollage() {
    return MasonryGridView.count(
      crossAxisCount: 3,
      mainAxisSpacing: 4,
      crossAxisSpacing: 4,
      itemCount: 9,
      padding: const EdgeInsets.all(8.0),
      itemBuilder: (context, index) => _collageTile(index),
    );
  }

  /// Texto de presentación.
  Widget _buildAboutText(BuildContext context, SizingInformation sizingInfo) {
    BusinessModel business = Provider.of<BusinessModel>(context);
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            // Titulo
            Text(
              '¿Print.Ideas3D?',
              textScaleFactor: ResponsiveHelper.textSizeMultiplier(sizingInfo),
              style: TextStyle(
                fontWeight: FontWeight.w500,
                color: kPrimaryColor,
                shadows: const [
                  Shadow(
                      blurRadius: 5,
                      color: Colors.black,
                      offset: Offset(2.0, 2.0))
                ],
                // Responsive font by design width.
                fontSize: Theme.of(context).textTheme.headlineMedium?.fontSize,
              ),
            ),
            // Descripcion
            Padding(
              padding: const EdgeInsets.only(top: 16.0),
              child: Center(
                child: Text(
                  business.aboutText,
                  textAlign: TextAlign.center,
                  textScaleFactor:
                      ResponsiveHelper.textSizeMultiplier(sizingInfo),
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    color: Colors.white,
                    // Responsive font by design width.
                    fontSize: Theme.of(context).textTheme.titleLarge?.fontSize,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (context, sizingInfo) {
      if (sizingInfo.isDesktop) {
        return Container(
          padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
          width: sizingInfo.isMobile || sizingInfo.isTablet
              ? double.infinity
              : sizingInfo.localWidgetSize.width * 0.7,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              // Collage de fotos sobre @print.ideas3d
              Expanded(
                child: _buildCollage(),
              ),
              // Texto que describe a @print.ideas3d
              Expanded(
                child: _buildAboutText(context, sizingInfo),
              ),
            ],
          ),
        );
      } else {
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // Collage de fotos sobre @print.ideas3d
            Expanded(
              flex: 2,
              child: _buildCollage(),
            ),
            // Texto que describe a @print.ideas3d
            Expanded(
              child: _buildAboutText(context, sizingInfo),
            ),
          ],
        );
      }
    });
  }
}
