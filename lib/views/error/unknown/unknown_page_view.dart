import 'package:flutter/material.dart';
import 'package:print_ideas_3d/constants/app_colors.dart';

class UnknownPageView extends StatelessWidget {
  const UnknownPageView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
      decoration: BoxDecoration(
          color: kPrimaryColor, borderRadius: BorderRadius.circular(8.0)),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              '404',
              style: TextStyle(
                color: Colors.black87,
                fontSize: Theme.of(context).textTheme.headlineSmall?.fontSize,
              ),
            ),
            Text(
              'Página no encontrada',
              style: TextStyle(
                color: Colors.black87,
                fontSize: Theme.of(context).textTheme.titleLarge?.fontSize,
              ),
            ),
          ],
        ),
      ),
    ));
  }
}
