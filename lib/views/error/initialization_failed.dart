import 'package:flutter/material.dart';

import '../../constants/app_colors.dart';

class InitializationError extends StatelessWidget {
  const InitializationError({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(color: kThemePrimaryColor),
        child: const Center(
          child: Text(
            'Oops! Hubo un error al cargar la página, intente nuevamente mas tarde',
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white70),
          ),
        ),
      ),
    );
  }
}
