import 'package:flutter/material.dart';
import 'package:print_ideas_3d/constants/app_colors.dart';
import 'package:print_ideas_3d/helpers/responsive_helper.dart';
import 'package:print_ideas_3d/models/core/business_model.dart';
import 'package:print_ideas_3d/router/ui_pages.dart';
import 'package:print_ideas_3d/widgets/home_card/home_card.dart';
import 'package:print_ideas_3d/widgets/platform_views/model_3d_viz.dart';
import 'package:print_ideas_3d/widgets/platform_views/pointer_interceptor_web.dart';
import 'package:provider/provider.dart';
import 'package:responsive_builder/responsive_builder.dart';

/// Página principal.
class HomeView extends StatelessWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (context, sizingInfo) {
      return SizedBox(
        child: Column(
          children: [
            // header
            Expanded(
              flex: 2,
              child: sizingInfo.isMobile
                  ? const HomeHeaderMobile()
                  : HomeHeaderDesktop(sizingInfo: sizingInfo),
            ),
            // content
            const Expanded(flex: 3, child: HomeContent())
          ],
        ),
      );
    });
  }
}

/// Home header for desktop.
class HomeHeaderDesktop extends StatelessWidget {
  const HomeHeaderDesktop({Key? key, required this.sizingInfo})
      : super(key: key);
  final SizingInformation sizingInfo;

  @override
  Widget build(BuildContext context) {
    final BusinessModel business = Provider.of<BusinessModel>(context);
    return Container(
      margin: const EdgeInsets.only(bottom: 8.0),
      child: Row(
        children: [
          Expanded(child: Home3DModel(modelId: business.model3dId)),
          Expanded(
            child: Text(
              'Materializa tus ideas a través del mundo de la impresión 3D.',
              textScaleFactor: ResponsiveHelper.textSizeMultiplier(sizingInfo),
              style: TextStyle(
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                  // Responsive font by design width.
                  fontSize: Theme.of(context).textTheme.headlineSmall?.fontSize,
                  shadows: const [
                    Shadow(
                        blurRadius: 5,
                        color: Colors.black,
                        offset: Offset(2.0, 2.0))
                  ]),
            ),
          )
        ],
      ),
    );
  }
}

/// Home header for mobiles.
class HomeHeaderMobile extends StatelessWidget {
  const HomeHeaderMobile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final BusinessModel business = Provider.of<BusinessModel>(context);
    return Container(
      margin: const EdgeInsets.only(bottom: 8.0),
      height: double.infinity,
      width: double.infinity,
      alignment: Alignment.center,
      decoration: const BoxDecoration(
        color: kThemePrimaryColor,
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Stack(children: [
          Home3DModel(modelId: business.model3dId),
          const DynamicPointerInterceptor()
        ]),
      ),
    );
  }
}

/// Muestra las principales caracteristicas del negocio.
class HomeContent extends StatelessWidget {
  const HomeContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, sizingInfo) {
        // Si estamos en un mobile:
        if (sizingInfo.isMobile) {
          return const HomeCard(
            cardImagePath: 'assets/images/services.jpg',
            cardTitle: 'Servicios',
            cardText:
                'Averigua los distintos servicios que ofrecemos y dale vida a tu proyecto.',
            destination: Pages.services,
          );
        }
        // Tablets & Desktops
        else {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 4.0),
            child: GridView.count(
              crossAxisCount: 3,
              mainAxisSpacing: 8.0,
              crossAxisSpacing: 8.0,
              childAspectRatio: sizingInfo.isTablet ? 0.75 : 1.5,
              children: const [
                // Services home card.
                HomeCard(
                  cardImagePath: 'assets/images/services.jpg',
                  cardTitle: 'Servicios',
                  cardText:
                      'Averigua los distintos servicios que ofrecemos y dale vida a tu proyecto.',
                  destination: Pages.services,
                ),
                // About home card.
                HomeCard(
                  cardImagePath: 'assets/images/about.jpg',
                  cardTitle: 'Conócenos',
                  cardText:
                      'Descubre quienes somos y la forma en la que hacemos nuestro trabajo.',
                  destination: Pages.about,
                ),
                // Works home card.
                HomeCard(
                  cardImagePath: 'assets/images/works.jpg',
                  cardTitle: 'Trabajos',
                  cardText:
                      'Hecha un vistazo a nuestros trabajos y alimenta tu imaginación para tu próximo proyecto.',
                  destination: Pages.works,
                )
              ],
            ),
          );
        }
      },
    );
  }
}
