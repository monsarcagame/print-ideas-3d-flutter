import 'package:flutter/material.dart';
import 'package:print_ideas_3d/constants/text_styles.dart';

class ServiceDetail extends StatelessWidget {
  const ServiceDetail({Key? key, required this.serviceId}) : super(key: key);

  final String serviceId;

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Text('Servicio: $serviceId',
            style: const TextStyle(
              fontSize: kTextSizeDefault,
            )));
  }
}
