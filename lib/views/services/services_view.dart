import 'package:auto_size_text_pk/auto_size_text_pk.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:print_ideas_3d/constants/text_styles.dart';
import 'package:print_ideas_3d/helpers/error_handling.dart';
import 'package:print_ideas_3d/models/core/app_state_model.dart';
import 'package:print_ideas_3d/models/service/service_model.dart';
import 'package:print_ideas_3d/widgets/dialogs/dialogs.dart';
import 'package:print_ideas_3d/widgets/image_uploader.dart';
import 'package:provider/provider.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:transparent_image/transparent_image.dart';

class ServicesView extends StatelessWidget {
  const ServicesView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Get app state.
    var appState = Provider.of<AppStateModel>(context, listen: false);
    if (appState.printUser.isSignedIn && appState.printUser.isAdmin) {
      // ADMIN.
      return Scaffold(
          body: const ServicesList(),
          floatingActionButton: FloatingActionButton(
              child: const Icon(Icons.add),
              onPressed: () async {
                // Agregar servicios con un dialog.
                await showAddServiceDialog(context);
              }));
    } else {
      return const Scaffold(body: ServicesList());
    }
  }
}

class ServicesList extends StatelessWidget {
  const ServicesList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<ServiceModel>?>(
      future: getServices(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(child: CircularProgressIndicator());
        } else if (snapshot.hasError) {
          return const Text('Error');
        } else if (snapshot.hasData) {
          return Container(child: _buildResponsiveServiceList(snapshot.data));
        }

        return const Text('no data');
      },
    );
  }

  /// Build responsive service list.
  Widget _buildResponsiveServiceList(List<ServiceModel>? data) {
    return ResponsiveBuilder(
      builder: (context, sizingInfo) {
        return GridView.count(
          childAspectRatio: 1.6,
          crossAxisCount: sizingInfo.isMobile ? 1 : 2,
          children: data!
              .map((serviceModel) =>
                  GridTile(child: ServiceCard(service: serviceModel)))
              .toList(),
        );
      },
    );
  }
}

/// Service card.
class ServiceCard extends StatelessWidget {
  final ServiceModel service;
  const ServiceCard({
    Key? key,
    required this.service,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (context, sizingInfo) {
      return Card(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // Service image.
            Expanded(
              child: SizedBox(
                width: double.infinity,
                height: double.infinity,
                child: ClipRRect(
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(4.0),
                      topRight: Radius.circular(4.0)),
                  child: FittedBox(
                    fit: BoxFit.cover,
                    child: FadeInImage.memoryNetwork(
                      image: service.images![0],
                      placeholder: kTransparentImage,
                      imageErrorBuilder: (context, exception, stackTrace) {
                        return Image.asset('images/missing_image.png');
                      },
                    ),
                  ),
                ),
              ),
            ),
            // Service detail.
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  // Service name.
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      service.name!,
                      style: TextStyle(
                        color: kTextColorCardTitle,
                        fontSize:
                            Theme.of(context).textTheme.titleMedium?.fontSize,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  // Service description.
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: AutoSizeText(
                      service.description!,
                      style: TextStyle(
                        color: kTextColorCardDefault,
                        fontSize:
                            Theme.of(context).textTheme.titleSmall?.fontSize,
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      );
    });
  }
}

/// Get services from Firestore.
Future<List<ServiceModel>?> getServices() async {
  List<ServiceModel> services = [];
  CollectionReference servicesCollection =
      FirebaseFirestore.instance.collection('services');

  try {
    QuerySnapshot servicesQuerySnapshot = await servicesCollection.get();

    services = servicesQuerySnapshot.docs
        .map((serviceSnapshot) => ServiceModel.fromJSON(
            id: serviceSnapshot.id,
            data: serviceSnapshot.data() as Map<String, dynamic>))
        .toList();

    return services;
  } on FirebaseException {
    return null;
  }
}

/// Agregar un servicio a la base de datos.
Future<void> addService(ServiceModel service) async {
  CollectionReference worksColRef =
      FirebaseFirestore.instance.collection('services');

  try {
    await worksColRef.add({
      'name': service.name,
      'description': service.description,
      'images': service.images
    });
  } on FirebaseException catch (e) {
    throw Failure(code: e.code, message: '${e.message}');
  }
}

/// Formulario para agregar un trabajo de impresión por medio de un dialogo.
Future<void> showAddServiceDialog(BuildContext context) async {
  return await showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        // Key for validate form inputs.
        final GlobalKey<FormState> formKey = GlobalKey<FormState>();
        // Controllers.
        final TextEditingController nameTextEditingCtrl =
            TextEditingController();
        final TextEditingController descriptionTextEditingCtrl =
            TextEditingController();
        String? serviceImageURL;

        return AlertDialog(
          scrollable: true,
          title: const Text('Agregar servicio'),
          content: Form(
              key: formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  // Service image uploader.
                  Container(
                    width: 100.0,
                    height: 100.0,
                    decoration: BoxDecoration(
                        border: Border.all(width: 2.0),
                        borderRadius: BorderRadius.circular(8.0)),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: ImageUploader(
                          path: 'images/services',
                          onImageUploaded: (imageUploadedURL) {
                            serviceImageURL = imageUploadedURL;
                          }),
                    ),
                  ),
                  // Name input.
                  TextFormField(
                    controller: nameTextEditingCtrl,
                    decoration:
                        const InputDecoration(hintText: 'Ingresa un nombre'),
                    validator: (value) => (value != null)
                        ? (value.isNotEmpty ? null : 'Campo requerido')
                        : null,
                  ),
                  // Description input.
                  TextFormField(
                    controller: descriptionTextEditingCtrl,
                    decoration: const InputDecoration(
                        hintText: 'Ingresa una descripción'),
                    validator: (value) => (value != null)
                        ? (value.isNotEmpty ? null : 'Campo requerido')
                        : null,
                  ),
                ],
              )),
          actions: [
            TextButton(
                onPressed: () async {
                  // Si el formulario es válido
                  if ((formKey.currentState != null
                          ? formKey.currentState!.validate()
                          : false) &&
                      serviceImageURL != null) {
                    // Intento agregar el trabajo de impresión en la base de datos.
                    try {
                      await addService(ServiceModel(
                          name: nameTextEditingCtrl.value.text,
                          description: descriptionTextEditingCtrl.value.text,
                          images: serviceImageURL != null
                              ? [serviceImageURL!]
                              : []));
                      await Dialogs.showMessageDialog(context,
                          message:
                              const Text('Servicio agregado correctamente'));
                      Navigator.of(context).pop();
                    } on Failure catch (failure) {
                      Dialogs.showMessageDialog(context,
                          message: Text('Error: ${failure.toString()}'),
                          alertType: AlertType.error);
                    }
                  }
                },
                child: const Text('Agregar'))
          ],
        );
      });
}
