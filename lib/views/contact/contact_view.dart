import 'package:auto_size_text_pk/auto_size_text_pk.dart';
import 'package:flutter/material.dart';
import 'package:print_ideas_3d/constants/app_colors.dart';
import 'package:print_ideas_3d/models/core/business_model.dart';
import 'package:provider/provider.dart';
import 'package:responsive_builder/responsive_builder.dart';

class ContactView extends StatelessWidget {
  const ContactView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var autoSizeGroup = AutoSizeGroup();
    return ResponsiveBuilder(builder: (context, sizingInfo) {
      return GridView.count(
        crossAxisCount: sizingInfo.isMobile
            ? 1
            : sizingInfo.isTablet
                ? 2
                : 4,
        childAspectRatio: sizingInfo.isMobile ? 1.5 : 1,
        children: [
          // Email
          ContactItem(
            contactIcon: 'assets/images/icons/icon_email.png',
            contactText: context.read<BusinessModel>().email,
            contactGroup: autoSizeGroup,
          ),
          ContactItem(
            contactIcon: 'assets/images/icons/icon_whatsapp.png',
            contactText: context.read<BusinessModel>().whatsappNumber,
            contactGroup: autoSizeGroup,
          ),
          ContactItem(
            contactIcon: 'assets/images/icons/icon_instagram.png',
            contactText: context.read<BusinessModel>().instagramPage,
            contactGroup: autoSizeGroup,
          ),
          ContactItem(
            contactIcon: 'assets/images/icons/icon_facebook.png',
            contactText: context.read<BusinessModel>().facebookPage,
            contactGroup: autoSizeGroup,
          ),
        ],
      );
    });
  }
}

/// Contact card.
class ContactItem extends StatelessWidget {
  final String contactText;
  final String contactIcon;
  final AutoSizeGroup contactGroup;
  const ContactItem({
    Key? key,
    required this.contactText,
    required this.contactIcon,
    required this.contactGroup,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: [
              // Contact icon.
              Container(
                padding: const EdgeInsets.all(12.0),
                decoration: BoxDecoration(
                  border: Border.all(width: 1.0),
                  color: kSecondaryColor,
                  shape: BoxShape.circle,
                  boxShadow: const [
                    BoxShadow(
                      color: Colors.black26,
                      offset: Offset(0, 2.0),
                      blurRadius: 6.0,
                    )
                  ],
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Image.asset(
                    contactIcon,
                    width: 80.0,
                    height: 80.0,
                  ),
                ),
              ),
              const SizedBox(height: 16.0),
              // Contact text.
              AutoSizeText(
                contactText,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: kPrimaryColor,
                  fontSize: Theme.of(context).textTheme.titleMedium?.fontSize,
                ),
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                group: contactGroup,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
