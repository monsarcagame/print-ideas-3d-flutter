import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:print_ideas_3d/helpers/error_handling.dart';
import 'package:print_ideas_3d/models/core/app_state_model.dart';
import 'package:print_ideas_3d/models/core/user_model.dart';
import 'package:print_ideas_3d/models/shop/cart_model.dart';
import 'package:print_ideas_3d/models/shop/merchant_order_model.dart';
import 'package:provider/provider.dart';

class MerchantOrdersView extends StatelessWidget {
  const MerchantOrdersView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var appState = Provider.of<AppStateModel>(context);
    return Scaffold(
        body: FutureBuilder<List<MerchantOrderModel>>(
            future: getMerchantOrders(appState.printUser),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const Center(child: LinearProgressIndicator());
              } else if (snapshot.hasError) {
                return const Text('Error');
              } else if (snapshot.hasData) {
                return Center(
                    child: MerchantOrdersList(ordersList: snapshot.data));
              }
              return const Text('No data!');
            }));
  }
}

class MerchantOrdersList extends StatelessWidget {
  const MerchantOrdersList({Key? key, this.ordersList}) : super(key: key);

  final List<MerchantOrderModel>? ordersList;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: ordersList?.length,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: ListTile(
                title: Text(ordersList![index].id),
                subtitle: Text(ordersList![index]
                    .orderCreationDate
                    .toDate()
                    .toLocal()
                    .toString()),
              ),
            ),
          );
        });
  }
}

/// Get merchant orders from database.
Future<List<MerchantOrderModel>> getMerchantOrders(PrintUser user) async {
  List<MerchantOrderModel> orders = [];
  CollectionReference ordersCollection =
      FirebaseFirestore.instance.collection('merchantOrders');
  try {
    QuerySnapshot querySnapshot = await ordersCollection
        .where('userId', isEqualTo: user.uid)
        .orderBy('orderCreationDate')
        .get();

    await Future.forEach<QueryDocumentSnapshot>(querySnapshot.docs,
        (docSnapshot) {
      // Document snapshot data.
      Map<String, dynamic> data = docSnapshot.data() as Map<String, dynamic>;
      // Primero obtengo los items de la orden.
      List<dynamic> listItems = data['items'];
      // Información de envío.
      Map<String, dynamic> shippingData = data['shipping'];

      orders.add(MerchantOrderModel(
          id: docSnapshot.id,
          userId: data['userId'],
          paymentStatus: EnumToString.fromString<PaymentStatus>(
                  PaymentStatus.values, data['paymentStatus']) ??
              PaymentStatus.expired,
          shipping: ShippingInformation(
              origin: Address.fromJSON(shippingData['shipping']['origin']),
              destination: CompleteAddress.fromJSON(
                  shippingData['shipping']['destination']),
              weight: shippingData['weight'],
              shippingCost: shippingData['shippingCost'],
              shippingMode: shippingData['shippingMode']),
          items: listItems
              .map((e) => MerchantOrderItem(
                  shopItemId: e['shopItemId'],
                  shopItemPrice: e['shopItemPrice'],
                  shopItemThumbnailURL: e['shopItemThumbnailURL'],
                  quantity: e['quantity']))
              .toList(),
          orderCreationDate: data['orderCreationDate']));
    });

    return orders;
  } on FirebaseException catch (e) {
    throw Failure(
      code: e.code,
      message: e.message ?? 'Hubo un error al intentar leer la base de datos',
    );
  }
}
