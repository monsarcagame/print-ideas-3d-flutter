import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:print_ideas_3d/constants/app_colors.dart';
import 'package:print_ideas_3d/constants/text_styles.dart';
import 'package:print_ideas_3d/helpers/error_handling.dart';
import 'package:print_ideas_3d/models/core/app_state_model.dart';
import 'package:print_ideas_3d/models/shop/cart_model.dart';
import 'package:print_ideas_3d/models/shop/shop_item_model.dart';
import 'package:print_ideas_3d/views/shop/shop_view.dart';
import 'package:print_ideas_3d/widgets/dialogs/dialogs.dart';
import 'package:provider/provider.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:intl/intl.dart';

class ProductDetailView extends StatelessWidget {
  const ProductDetailView({Key? key, required this.productId})
      : super(key: key);

  final String productId;

  @override
  Widget build(BuildContext context) {
    // Get Cart and app state by provider.
    final shopCart = Provider.of<Cart>(context);
    final appState = Provider.of<AppStateModel>(context, listen: false);

    // Price Formater.
    var pesosFormat = NumberFormat.currency(symbol: "\$");

    // Font size multiplier.
    var fontSizeMultiplier = 1;

    return Center(
      child: FutureBuilder<ShopItemModel>(
        future: getProduct(productId),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: LinearProgressIndicator(),
            );
          } else if (snapshot.hasError) {
            Dialogs.showMessageDialog(
              context,
              message: Text(
                snapshot.error!.toString(),
              ),
              alertType: AlertType.error,
            );
          } else if (snapshot.hasData) {
            // ShopItemModel.
            ShopItemModel shopItem = snapshot.data!;
            //
            return Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                // Image
                Expanded(
                  flex: 2,
                  child: ClipRRect(
                    child: FittedBox(
                      fit: BoxFit.cover,
                      child: FadeInImage.memoryNetwork(
                        placeholder: kTransparentImage,
                        image: shopItem.imageURL,
                        imageErrorBuilder: (context, exception, stackTrace) {
                          return Image.asset('assets/images/missing_image.png');
                        },
                      ),
                    ),
                  ),
                ),
                // Detail
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // Title
                        Text(
                          snapshot.data!.title,
                          style: TextStyle(
                            fontSize: Theme.of(context)
                                .textTheme
                                .titleLarge
                                ?.fontSize,
                            color: kPrimaryColor,
                          ),
                        ),
                        const SizedBox(
                          height: 8.0,
                        ),
                        // Material composition.
                        RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: 'Material: ',
                                style: TextStyle(
                                  fontSize: Theme.of(context)
                                      .textTheme
                                      .bodyLarge
                                      ?.fontSize,
                                  color: Colors.white,
                                ),
                              ),
                              // Material
                              TextSpan(
                                text: snapshot.data!.material,
                                style: TextStyle(
                                  fontSize: Theme.of(context)
                                      .textTheme
                                      .bodyLarge
                                      ?.fontSize,
                                  color: kSecondaryColor,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 8.0,
                        ),
                        // Item price.
                        Text(
                          pesosFormat.format(snapshot.data!.price),
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontSize:
                                kTextSizeShopItemPrice * fontSizeMultiplier,
                            fontWeight: FontWeight.bold,
                            color: kTextColorShopItemPrice,
                          ),
                        ),
                        const SizedBox(
                          height: 8.0,
                        ),
                        // Description
                        Text(
                          snapshot.data!.description,
                          style: TextStyle(
                            fontSize: Theme.of(context)
                                .textTheme
                                .bodyMedium
                                ?.fontSize,
                            color: Colors.white70,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                // Add to cart
                Expanded(
                  child: AddToCartButton(
                    item: snapshot.data!,
                    appState: appState,
                    shopCart: shopCart,
                    fontSizeMultiplier: 1,
                  ),
                ),
              ],
            );
          }

          return const Text('No data!');
        },
      ),
    );
  }
}

/// Get product from database.
Future<ShopItemModel> getProduct(String id) async {
  try {
    DocumentSnapshot productDocSnapshot =
        await FirebaseFirestore.instance.collection('shopItems').doc(id).get();
    Map<String, dynamic> data =
        productDocSnapshot.data() as Map<String, dynamic>;

    return ShopItemModel.fromJSON(id: id, data: data);
  } on FirebaseException catch (e) {
    throw Failure(code: e.code, message: e.message ?? 'Hubo un error!');
  }
}
