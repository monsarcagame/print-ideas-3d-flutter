import 'package:flutter/material.dart';
import 'package:print_ideas_3d/constants/app_colors.dart';
import 'package:print_ideas_3d/helpers/form_validation_helper.dart';
import 'package:print_ideas_3d/helpers/shipping_helpers.dart';
import 'package:print_ideas_3d/helpers/url_helper.dart';
import 'package:print_ideas_3d/models/core/app_state_model.dart';
import 'package:print_ideas_3d/models/shop/cart_model.dart';
import 'package:print_ideas_3d/models/shop/shop_item_model.dart';
import 'package:print_ideas_3d/services/firebase_service.dart';
import 'package:print_ideas_3d/widgets/dialogs/dialogs.dart';
import 'package:provider/provider.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:select_form_field/select_form_field.dart';

class CartView extends StatelessWidget {
  const CartView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (context, sizingInfo) {
      return Center(
        child: Container(
          constraints: BoxConstraints(
              maxWidth: sizingInfo.isMobile || sizingInfo.isTablet
                  ? double.infinity
                  : sizingInfo.screenSize.width * 0.6),
          child: const Column(
            children: [
              // Cart items zone.
              Expanded(
                flex: 4,
                child: CartList(),
              ),
              // Checkout zone.
              Expanded(flex: 2, child: CheckoutSection())
            ],
          ),
        ),
      );
    });
  }
}

/// Lista los ítems que contiene el carrito.
class CartList extends StatelessWidget {
  const CartList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var cart = context.watch<Cart>();
    return ListView(
      children: cart.items
          .map((item) => CartItemCard(
                cart: cart,
                item: item,
              ))
          .toList(),
    );
  }
}

/// Card con información y acciones para modificar el ítem.
class CartItemCard extends StatelessWidget {
  const CartItemCard({
    Key? key,
    required this.cart,
    required this.item,
  }) : super(key: key);

  final ShopItemModel item;
  final Cart cart;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 4.0,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: StatefulBuilder(
          builder: (BuildContext context, setState) {
            return Row(children: [
              // Item image.
              SizedBox(
                width: 80.0,
                height: 80.0,
                child: ClipRRect(
                  borderRadius: const BorderRadius.all(Radius.circular(4.0)),
                  child: FittedBox(
                    fit: BoxFit.cover,
                    child: Image.network(item.imageURL),
                  ),
                ),
              ),
              // Item info.
              Expanded(
                flex: 2,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // Item title.
                      Text(
                        item.title,
                        style: TextStyle(
                          fontSize:
                              Theme.of(context).textTheme.titleMedium?.fontSize,
                          color: kPrimaryColor,
                        ),
                      ),
                      // Precio unitario.
                      Text(
                        'Price: \$${item.price.toString()}',
                        style: TextStyle(
                          fontSize:
                              Theme.of(context).textTheme.titleSmall?.fontSize,
                          color: Colors.white,
                        ),
                      ),
                      // Item count modifier.
                      Row(
                        children: [
                          IconButton(
                              onPressed: () {
                                setState(() {
                                  cart.decrementItemCount(item);
                                });
                              },
                              icon: const Icon(Icons.remove_circle)),
                          Text('${cart.getItemCount(item)}',
                              style: const TextStyle(color: Colors.white)),
                          IconButton(
                              onPressed: () {
                                setState(() {
                                  cart.incrementItemCount(item);
                                });
                              },
                              icon: const Icon(Icons.add_circle))
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              // Item subtotal.
              Expanded(
                child: Text(
                  'Subtotal: \$${cart.subtotalItem(item).toString()}',
                  style: TextStyle(
                    fontSize: Theme.of(context).textTheme.titleSmall?.fontSize,
                    color: Colors.white,
                  ),
                ),
              ),
              // Item actions.
              Expanded(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      IconButton(
                        icon: const Icon(Icons.delete_outline_rounded),
                        onPressed: () => cart.removeFromCart(item),
                      )
                    ]),
              )
            ]);
          },
        ),
      ),
    );
  }
}

/// Checkout section.
class CheckoutSection extends StatelessWidget {
  const CheckoutSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Key para poder cerrar el loading dialog.
    GlobalKey<State> keyLoader = GlobalKey<State>();
    // App state reference.
    var appState = Provider.of<AppStateModel>(context);

    return Consumer<Cart>(builder: (context, cart, child) {
      return Card(
        elevation: 4.0,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // Cart summary.
              Expanded(
                flex: 4,
                child: Column(children: [
                  // Poducts subtotal.
                  Expanded(
                    child: Row(children: [
                      const Expanded(
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(0.0, 4.0, 16.0, 4.0),
                          child: Text('Subtotal',
                              style: TextStyle(color: Colors.white)),
                        ),
                      ),
                      Expanded(
                          flex: 4,
                          child: Text('\$${cart.total}',
                              style: const TextStyle(color: Colors.white))),
                    ]),
                  ),
                  // Shippment.
                  Expanded(
                    child: Row(children: [
                      // Label.
                      const Expanded(
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(0.0, 4.0, 16.0, 4.0),
                          child: Text('Envío',
                              style: TextStyle(color: Colors.white)),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            // Shipping cost.
                            Expanded(
                                child: Text('\$${cart.shippingCost}',
                                    style:
                                        const TextStyle(color: Colors.white))),
                            // Shipping mode selection.
                            Expanded(
                              flex: 2,
                              child: Center(
                                child: DropdownButton<ShippingMode>(
                                  value: cart.shippingInfo.shippingMode,
                                  icon:
                                      const Icon(Icons.arrow_downward_rounded),
                                  style: const TextStyle(color: kPrimaryColor),
                                  underline: Container(
                                    height: 2.0,
                                    color: kPrimaryColor,
                                  ),
                                  items: const [
                                    DropdownMenuItem(
                                      value: ShippingMode.localPickup,
                                      child: Text('Retirar por local',
                                          style:
                                              TextStyle(color: Colors.white)),
                                    ),
                                    DropdownMenuItem(
                                      value: ShippingMode.sucursalPickup,
                                      child: Text('Retirar por sucursal',
                                          style:
                                              TextStyle(color: Colors.white)),
                                    ),
                                    DropdownMenuItem(
                                      value: ShippingMode.homeDelivery,
                                      child: Text('Envio a domicilio',
                                          style:
                                              TextStyle(color: Colors.white)),
                                    ),
                                  ],
                                  onChanged: (value) {
                                    cart.updateShippingInformation(
                                        newShippingMode: value as ShippingMode);
                                    // Mostrar advertencia por falta de información de envío.
                                    if (value != ShippingMode.localPickup &&
                                        cart.shippingInfo.destination == null) {
                                      Dialogs.showMessageDialog(
                                        context,
                                        message: const Text(
                                            'Completar información de envío'),
                                        alertType: AlertType.info,
                                      );
                                    }
                                  },
                                ),
                              ),
                            ),
                            // Load shipping info button.
                            Expanded(
                              flex: 2,
                              child: Center(
                                child: ElevatedButton.icon(
                                    onPressed: () {
                                      showShippingFormDialog(context);
                                    },
                                    icon: const Icon(Icons.local_shipping),
                                    label: const Text('Datos de envío')),
                              ),
                            )
                          ],
                        ),
                      ),
                    ]),
                  ),
                ]),
              ),
              // Checkout.
              Expanded(
                child: Container(
                  decoration: const BoxDecoration(
                      border: Border(
                          top: BorderSide(width: 1.0, color: kPrimaryColor))),
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        // Costo total.
                        Expanded(
                            child: Text('\$${cart.totalWithShipping}',
                                textAlign: TextAlign.center,
                                style: const TextStyle(
                                    color: Colors.lightGreen,
                                    fontWeight: FontWeight.w500))),
                        // Button.
                        Expanded(
                          child: ElevatedButton(
                            onPressed: () {
                              if (cart.validateForCheckout()) {
                                Dialogs.showLoadingDialog(context,
                                    key: keyLoader,
                                    loadingMessage: 'Procesando...');
                                FirebaseService()
                                    .getMercadoPagoPreference(
                                        appState.printUser, cart)
                                    .then((initPoint) {
                                  Navigator.of(keyLoader.currentContext!,
                                          rootNavigator: true)
                                      .pop();
                                  UrlHelper.launchURL(initPoint);
                                });
                              }
                            },
                            child: const Text(
                              'Checkout',
                              style: TextStyle(),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      );
    });
  }
}

/// Mostrar formulario de envío en un dialog.
Future<void> showShippingFormDialog(BuildContext context) async {
  return await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        var cart = context.watch<Cart>();
        final GlobalKey<FormState> formKey = GlobalKey<FormState>();
        final TextEditingController contactController =
            TextEditingController(text: cart.shippingInfo.destination?.contact);
        final TextEditingController zipCodeController =
            TextEditingController(text: cart.shippingInfo.destination?.zipCode);
        final TextEditingController streetController =
            TextEditingController(text: cart.shippingInfo.destination?.street);
        final TextEditingController numberController = TextEditingController(
            text: cart.shippingInfo.destination?.number.toString());
        final TextEditingController stateController =
            TextEditingController(text: cart.shippingInfo.destination?.state);
        final TextEditingController cityController =
            TextEditingController(text: cart.shippingInfo.destination?.city);
        final TextEditingController floorController = TextEditingController();
        final TextEditingController apartmentController =
            TextEditingController();
        final TextEditingController emailController = TextEditingController();

        return AlertDialog(
          scrollable: true,
          title: const Text('Datos de envío'),
          content: StatefulBuilder(builder: (context, setState) {
            return Form(
                key: formKey,
                child: Column(mainAxisSize: MainAxisSize.min, children: [
                  // Nombre y apellido.
                  TextFormField(
                      decoration:
                          const InputDecoration(labelText: 'Nombre y Apellido'),
                      keyboardType: TextInputType.name,
                      controller: contactController,
                      validator: (value) {
                        return value!.isNotEmpty ? null : 'Ingresar un nombre';
                      }),
                  // Código postal.
                  TextFormField(
                      decoration:
                          const InputDecoration(labelText: 'Código postal'),
                      keyboardType: TextInputType.number,
                      controller: zipCodeController,
                      validator: (value) {
                        return value!.isNotEmpty
                            ? null
                            : 'Ingresar un código postal';
                      }),
                  // Street.
                  TextFormField(
                      decoration: const InputDecoration(labelText: 'Calle'),
                      keyboardType: TextInputType.streetAddress,
                      controller: streetController,
                      validator: (value) {
                        return value!.isNotEmpty ? null : 'Ingresar una calle';
                      }),
                  // Street number.
                  TextFormField(
                      decoration: const InputDecoration(labelText: 'Número'),
                      keyboardType: TextInputType.number,
                      controller: numberController,
                      validator: (value) {
                        return value!.isNotEmpty ? null : 'Ingresar un número';
                      }),
                  // State.
                  SelectFormField(
                      items: shippingStates
                          .map((state) => {'value': state, 'label': state})
                          .toList(),
                      decoration: const InputDecoration(labelText: 'Provincia'),
                      keyboardType: TextInputType.text,
                      controller: stateController,
                      validator: (value) {
                        return value!.isNotEmpty
                            ? null
                            : 'Ingresar una provincia';
                      }),
                  // City.
                  TextFormField(
                      decoration: const InputDecoration(labelText: 'Ciudad'),
                      keyboardType: TextInputType.text,
                      controller: cityController,
                      validator: (value) {
                        return value!.isNotEmpty ? null : 'Ingresar una ciudad';
                      }),
                  // Floor.
                  TextFormField(
                      decoration: const InputDecoration(labelText: 'Piso'),
                      keyboardType: TextInputType.text,
                      controller: floorController,
                      validator: (value) {
                        return null;
                      }),
                  // Departamento.
                  TextFormField(
                      decoration:
                          const InputDecoration(labelText: 'Departamento'),
                      keyboardType: TextInputType.text,
                      controller: apartmentController,
                      validator: (value) {
                        return null;
                      }),
                  // Email.
                  TextFormField(
                      decoration: const InputDecoration(
                          labelText: 'Correo electrónico'),
                      keyboardType: TextInputType.emailAddress,
                      controller: emailController,
                      validator: (value) {
                        return value!.isEmpty
                            ? 'Campo requerido'
                            : FormValidationHelper.validateEmail(value.trim());
                      })
                ]));
          }),
          actions: [
            ElevatedButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text('Cancelar')),
            ElevatedButton(
                onPressed: () {
                  if (formKey.currentState!.validate()) {
                    var cart = Provider.of<Cart>(context, listen: false);
                    cart
                        .updateShippingInformation(
                      newDestination: CompleteAddress(
                          contact: contactController.value.text,
                          zipCode: zipCodeController.value.text,
                          street: streetController.value.text,
                          number: int.parse(numberController.value.text),
                          state: stateController.value.text,
                          city: cityController.value.text,
                          floor: floorController.value.text,
                          apartment: apartmentController.value.text,
                          email: emailController.value.text),
                    )
                        .then((value) {
                      Navigator.of(context).pop();
                    });
                  }
                },
                child: const Text('Listo'))
          ],
        );
      });
}
