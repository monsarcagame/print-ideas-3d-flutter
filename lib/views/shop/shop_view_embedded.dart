import 'dart:html';

import 'package:flutter/material.dart';
import 'dart:ui' as ui;

import 'package:print_ideas_3d/widgets/platform_views/pointer_interceptor_web.dart';

class ShopViewEmbedded extends StatelessWidget {
  const ShopViewEmbedded({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [shopPlatformView(), const DynamicPointerInterceptor()],
      ),
    );
  }
}

Widget shopPlatformView() {
  // ignore: undefined_prefixed_name
  ui.platformViewRegistry.registerViewFactory("shop-html", (int uid) {
    final element = IFrameElement()
      ..style.width = "100%"
      ..style.height = "100%"
      ..style.border = "none"
      ..src = "https://printideas3d.empretienda.com.ar/";

    return element;
  });

  return const HtmlElementView(viewType: "shop-html");
}
