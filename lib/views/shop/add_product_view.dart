import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:flutter/material.dart';
import 'package:file_picker/file_picker.dart';
import 'package:pattern_formatter/pattern_formatter.dart';
import 'package:print_ideas_3d/constants/app_colors.dart';
import 'package:print_ideas_3d/constants/text_styles.dart';
import 'package:print_ideas_3d/helpers/color_helper.dart';
import 'package:print_ideas_3d/models/shop/shop_item_model.dart';
import 'package:print_ideas_3d/services/firebase_service.dart';
import 'package:print_ideas_3d/widgets/dialogs/dialogs.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:select_form_field/select_form_field.dart';
import 'package:flutter_multi_formatter/flutter_multi_formatter.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';

class AddProductView extends StatelessWidget {
  const AddProductView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Map<String, List<String>>>(
        future: FirebaseService().getCategories(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          } else if (snapshot.hasError) {
            return Text(snapshot.error.toString());
          } else if (snapshot.hasData) {
            // Get map of categories.
            var categories = snapshot.data;
            return AddProductForm(categories: categories);
          } else {
            return const Text('No se pudieron abtener las categorias.');
          }
        });
  }
}

/// Formulario para agregar un nuevo producto.
class AddProductForm extends StatefulWidget {
  final Map<String, List<String>>? categories;

  const AddProductForm({Key? key, required this.categories}) : super(key: key);

  @override
  _AddProductFormState createState() => _AddProductFormState();
}

class _AddProductFormState extends State<AddProductForm> {
  // Form key.
  final _formKey = GlobalKey<FormState>();
  // Controllers.
  final _titleController = TextEditingController();
  final _shortInfoController = TextEditingController();
  final _descriptionController = TextEditingController();
  final _priceController =
      TextEditingController(text: '${MoneySymbols.DOLLAR_SIGN} 0.00');
  final _categoryController = TextEditingController();
  final _subcategoryController = TextEditingController();
  final _printingTimeController = TextEditingController(text: '0.00');
  final _stockController = TextEditingController(text: '1');
  final _statusController = TextEditingController();
  final _widthController = TextEditingController(text: '1.0');
  final _heightController = TextEditingController(text: '1.0');
  final _lengthController = TextEditingController(text: '1.0');
  final _massController = TextEditingController(text: '1.0');
  // Categories & Subcategories items.
  final List<Map<String, dynamic>> _categoryItems = [];
  List<Map<String, dynamic>> subcategoriesItems = [];
  String? categorySelected;
  // Image upload state.
  bool isUploading = false;
  Image? imageUploaded;
  String? imageUploadedURL;
  double imageUploadProgress = 0;
  // Color picker.
  Color? currentColor = Colors.white;

  // Submit product data to firestore.
  void _submit() {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      // Create ShopItemModel to upload to firestore.
      ShopItemModel shopItem = ShopItemModel(
          id: '',
          title: _titleController.value.text,
          shortInfo: _shortInfoController.value.text,
          publishedDate: Timestamp.now(),
          description: _descriptionController.value.text,
          price: double.parse(toNumericString(_priceController.value.text,
              allowPeriod: true, allowHyphen: false)),
          url: 'https://printideas3d.online/#/shop',
          imageURL: imageUploadedURL ?? '',
          thumbnailURL: '',
          status: _statusController.value.text,
          category: ShopItemCategory(
              name: _categoryController.value.text,
              subcategory: _subcategoryController.value.text),
          printingTime: double.parse(_printingTimeController.value.text),
          stock: int.parse(_stockController.value.text),
          color: ColorHelper.getHexStringFromColor(currentColor),
          width: double.parse(_widthController.value.text),
          height: double.parse(_heightController.value.text),
          length: double.parse(_lengthController.value.text),
          mass: double.parse(_massController.value.text));
      // Add shop item to firebase.
      FirebaseService().addShopItem(shopItem).then((value) {
        value.fold(
          (l) => Dialogs.showMessageDialog(context,
              message: const Text('Ítem agregado correctamente'), action: () {
            // Remove AddProduct route from navigator.
            Navigator.pop(context);
          }),
          (r) => Dialogs.showMessageDialog(
            context,
            message: Text(r.message ?? 'Hubo un error!'),
            alertType: AlertType.error,
          ),
        );
      });
    }
  }

  @override
  void initState() {
    super.initState();
    // Build list of categories for select form input.
    widget.categories!.forEach((key, value) {
      _categoryItems.add({
        'value': key,
        'label': key,
      });
    });
  }

  @override
  void dispose() {
    _titleController.dispose();
    _shortInfoController.dispose();
    _descriptionController.dispose();
    _priceController.dispose();
    _categoryController.dispose();
    _subcategoryController.dispose();
    _printingTimeController.dispose();
    _stockController.dispose();
    _statusController.dispose();
    _widthController.dispose();
    _heightController.dispose();
    _lengthController.dispose();
    _massController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (context, sizingInfo) {
      // Compute form width.
      var formWidth = sizingInfo.isMobile
          ? double.infinity
          : sizingInfo.isTablet
              ? sizingInfo.screenSize.width * 0.9
              : sizingInfo.screenSize.width * 0.5;
      var inputSpacing = 8.0;

      return Center(
        child: SizedBox(
          width: formWidth,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Product image.
                Container(
                  width: 100.0,
                  height: 100.0,
                  decoration: const BoxDecoration(color: kSecondaryColor),
                  child:
                      Stack(alignment: AlignmentDirectional.center, children: [
                    Container(
                      child: imageUploaded,
                    ),
                    // Add photo button.
                    Visibility(
                      visible: imageUploaded == null,
                      child: IconButton(
                          icon: const Icon(
                            Icons.add_a_photo_outlined,
                            color: kPrimaryColor,
                          ),
                          onPressed: () async {
                            FilePickerResult? result = await FilePicker.platform
                                .pickFiles(type: FileType.image);
                            if (result != null) {
                              // Upload image to firebase storage.
                              FirebaseService().uploadImage(
                                  result.files.first.bytes!,
                                  'images/shop/${result.files.first.name}',
                                  (firebase_storage.TaskSnapshot event) {
                                setState(() {
                                  isUploading = event.state ==
                                      firebase_storage.TaskState.running;
                                  imageUploadProgress =
                                      event.bytesTransferred / event.totalBytes;
                                });
                              }).then((either) {
                                either.fold((imageURL) {
                                  setState(() {
                                    imageUploadedURL = imageURL;
                                    imageUploaded = Image.network(imageURL);
                                  });
                                }, (e) {
                                  setState(() {
                                    // Handle image upload error.
                                    imageUploadedURL = null;
                                    imageUploaded = null;
                                    Dialogs.showMessageDialog(
                                      context,
                                      message: Text(e.toString()),
                                      alertType: AlertType.error,
                                    );
                                  });
                                });
                              });
                            }
                          }),
                    ),
                    // Image upload progress bar.
                    Visibility(
                      visible: isUploading,
                      child: CircularProgressIndicator(
                        value: imageUploadProgress,
                      ),
                    )
                  ]),
                ),
                // Separator.
                SizedBox(height: inputSpacing),
                // Product form.
                Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        // Title.
                        TextFormField(
                          controller: _titleController,
                          style: kTextFormFieldStyle,
                          decoration: const InputDecoration(
                              labelText: 'Title',
                              labelStyle: kTextFormLabelStyle,
                              hintText: 'Product title.'),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'El título no puede estar vacio!';
                            }
                            return null;
                          },
                        ),
                        // Input separation.
                        SizedBox(height: inputSpacing),
                        // ShortInfo.
                        TextFormField(
                          controller: _shortInfoController,
                          style: kTextFormFieldStyle,
                          decoration: const InputDecoration(
                              labelText: 'Short Info',
                              labelStyle: kTextFormLabelStyle,
                              hintText: 'Short Info'),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'La información corta no puede estar vacía!';
                            }
                            return null;
                          },
                        ),
                        // Input separation.
                        SizedBox(height: inputSpacing),
                        // Description.
                        TextFormField(
                          controller: _descriptionController,
                          style: kTextFormFieldStyle,
                          decoration: const InputDecoration(
                              labelText: 'Description',
                              labelStyle: kTextFormLabelStyle,
                              hintText: 'Description'),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'La descripción no puede estar vacía!';
                            }
                            return null;
                          },
                        ),
                        // Input separation.
                        SizedBox(height: inputSpacing),
                        // Price.
                        TextFormField(
                          controller: _priceController,
                          style: kTextFormFieldStyle,
                          decoration: const InputDecoration(
                              labelText: 'Precio',
                              labelStyle: kTextFormLabelStyle,
                              hintText: 'Precio',
                              helperText: 'Precio unitario del producto.'),
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            CurrencyInputFormatter(
                                leadingSymbol: MoneySymbols.DOLLAR_SIGN,
                                useSymbolPadding: true)
                          ],
                          validator: (value) {
                            if (double.parse(
                                    toNumericString(value, allowPeriod: true)) <
                                0) {
                              return 'Ingresar un valor positivo!';
                            }
                            return null;
                          },
                        ),
                        // Input separation.
                        SizedBox(height: inputSpacing),
                        // Printing Time.
                        TextFormField(
                          controller: _printingTimeController,
                          style: kTextFormFieldStyle,
                          decoration: const InputDecoration(
                              labelText: 'Tiempo de impresión',
                              labelStyle: kTextFormLabelStyle,
                              helperText: 'Tiempo de impresión en horas.'),
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            ThousandsFormatter(allowFraction: true)
                          ],
                          validator: (value) {
                            if (double.parse(value!) <= 0) {
                              return 'Ingresar un valor mayor a cero!';
                            }
                            return null;
                          },
                        ),
                        // Input separation.
                        SizedBox(height: inputSpacing),
                        // Stock.
                        TextFormField(
                          controller: _stockController,
                          style: kTextFormFieldStyle,
                          decoration: const InputDecoration(
                              labelText: 'Stock',
                              labelStyle: kTextFormLabelStyle,
                              hintText: 'Stock'),
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            ThousandsFormatter(allowFraction: false)
                          ],
                          validator: (value) {
                            if (int.parse(value!) <= 0) {
                              return 'Ingresar un valor mayor a cero!';
                            }
                            return null;
                          },
                        ),
                        // Input separation.
                        SizedBox(height: inputSpacing),
                        // Color input.
                        ColorFormField(
                            decoration: const InputDecoration(
                                labelText: 'Color',
                                labelStyle: kTextFormLabelStyle),
                            onSaved: (Color? color) {
                              currentColor = color;
                            }),
                        // Input separation.
                        SizedBox(height: inputSpacing),
                        // Status selection.
                        SelectFormField(
                          type: SelectFormFieldType.dropdown,
                          dialogTitle: 'Status',
                          dialogCancelBtn: 'Cancel',
                          controller: _statusController,
                          style: kTextFormFieldStyle,
                          decoration: const InputDecoration(
                            labelText: 'Status',
                            labelStyle: kTextFormLabelStyle,
                            hintText: 'Elige el estado del producto',
                          ),
                          items: ProductStatus.values
                              .map((e) => {
                                    'value': itemAvailability[e],
                                    'label': itemAvailability[e]
                                  })
                              .toList(),
                        ),
                        SizedBox(height: inputSpacing),
                        // Category selection.
                        SelectFormField(
                          type: SelectFormFieldType.dialog,
                          dialogTitle: 'Categories',
                          dialogCancelBtn: 'Cancel',
                          controller: _categoryController,
                          style: kTextFormFieldStyle,
                          decoration: const InputDecoration(
                            labelText: 'Category',
                            labelStyle: kTextFormLabelStyle,
                            hintText: 'Select a category',
                          ),
                          items: _categoryItems,
                          onChanged: (value) {
                            if (value != categorySelected) {
                              setState(() {
                                _subcategoryController.clear();
                                categorySelected = value;
                                subcategoriesItems = widget.categories![value]!
                                    .map((subcategory) => {
                                          'value': subcategory,
                                          'label': subcategory
                                        })
                                    .toList();
                              });
                            }
                          },
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Categoria no puede estar vacía.';
                            }
                            return null;
                          },
                        ),
                        // Input separation.
                        SizedBox(height: inputSpacing),
                        // Subcategory selection.
                        SelectFormField(
                            type: SelectFormFieldType.dialog,
                            dialogTitle: 'Subcategories',
                            dialogCancelBtn: 'Cancel',
                            controller: _subcategoryController,
                            style: kTextFormFieldStyle,
                            decoration: const InputDecoration(
                              labelText: 'Subcategory',
                              labelStyle: kTextFormLabelStyle,
                              hintText: 'Select a subcategory',
                            ),
                            items: subcategoriesItems),
                        // Input separation.
                        SizedBox(height: inputSpacing),
                      ],
                    )),
                // Add product button.
                TextButton(
                  child: Container(
                    height: 40.0,
                    width: 100.0,
                    decoration: BoxDecoration(
                      color: Colors.green.withOpacity(0.2),
                    ),
                    child: const Center(child: Text('Add product')),
                  ),
                  onPressed: () {
                    _submit();
                  },
                )
              ],
            ),
          ),
        ),
      );
    });
  }
}

/// Color input for forms.
class ColorFormField extends FormField<Color> {
  ColorFormField(
      {Key? key,
      FormFieldSetter<Color>? onSaved,
      FormFieldValidator<Color>? validator,
      Color initialValue = Colors.white,
      InputDecoration? decoration,
      AutovalidateMode autovalidateMode = AutovalidateMode.disabled})
      : super(
            key: key,
            onSaved: onSaved,
            validator: validator,
            initialValue: initialValue,
            autovalidateMode: autovalidateMode,
            builder: (FormFieldState<Color> state) {
              return InputDecorator(
                  decoration:
                      (decoration ?? const InputDecoration(labelText: 'Color')),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: Row(
                      children: [
                        Container(
                            width: 16.0, height: 16.0, color: state.value),
                        const SizedBox(width: 10.0),
                        TextButton(
                          onPressed: () {
                            showDialog(
                                context: state.context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                      titlePadding: const EdgeInsets.all(0.0),
                                      contentPadding: const EdgeInsets.all(0.0),
                                      content: SingleChildScrollView(
                                        child: ColorPicker(
                                          pickerColor: state.value!,
                                          onColorChanged: (Color color) {
                                            // Change color state.
                                            state.didChange(color);
                                          },
                                          colorPickerWidth: 300.0,
                                          pickerAreaHeightPercent: 0.7,
                                          enableAlpha: true,
                                          displayThumbColor: true,
                                          paletteType: PaletteType.hsv,
                                          pickerAreaBorderRadius:
                                              const BorderRadius.only(
                                            topLeft: Radius.circular(2.0),
                                            topRight: Radius.circular(2.0),
                                          ),
                                        ),
                                      ));
                                });
                          },
                          child: const Text('Pick color'),
                        )
                      ],
                    ),
                  ));
            });
}
