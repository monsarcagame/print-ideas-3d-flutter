import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:print_ideas_3d/constants/constants.dart';
import 'package:print_ideas_3d/constants/text_styles.dart';
import 'package:print_ideas_3d/helpers/responsive_helper.dart';
import 'package:print_ideas_3d/models/core/app_state_model.dart';
import 'package:print_ideas_3d/models/shop/cart_model.dart';
import 'package:print_ideas_3d/models/shop/shop_item_model.dart';
import 'package:print_ideas_3d/router/ui_pages.dart';
import 'package:print_ideas_3d/services/firebase_service.dart';
import 'package:print_ideas_3d/widgets/dialogs/dialogs.dart';
import 'package:provider/provider.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:intl/intl.dart';
import 'package:transparent_image/transparent_image.dart';

class ShopView extends StatelessWidget {
  const ShopView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Get app state.
    final appState = Provider.of<AppStateModel>(context, listen: false);

    if (appState.printUser.isSignedIn && appState.printUser.isAdmin) {
      // Admin.
      return Scaffold(
        body: const ShopItemList(),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.green,
          onPressed: () {
            // Modifico la app state para que la página actual sea AddProductView.
            appState.currentPage = Pages.addProduct;
          },
          child: const Icon(Icons.add),
        ),
      );
    } else {
      // Common user and anonymous.
      return const Scaffold(
        body: ShopItemList(),
      );
    }
  }
}

/// Shop item list.
class ShopItemList extends StatelessWidget {
  const ShopItemList({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
        stream: FirebaseService().getShopItemsByDateStream(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            // Wait for shop data.
            return const Center(child: CircularProgressIndicator());
          } else if (snapshot.hasError) {
            return const Text('Hubo un error!!');
          } else if (snapshot.hasData) {
            return _buildResponsiveItemList(snapshot.data!.docs
                .map((snapshot) => ShopItemModel.fromJSON(
                    id: snapshot.id,
                    data: snapshot.data() as Map<String, dynamic>))
                .toList());
          }
          // No hay nada para mostrar.
          return const Center(
              child: Text('El shop está vacío',
                  style: TextStyle(color: Colors.white)));
        });
  }

  // Construir la lista de ítems del shop y que se adapte al tamaño de pantalla.
  Widget _buildResponsiveItemList(List<ShopItemModel> shopItemList) {
    return ResponsiveBuilder(
      builder: (context, sizingInfo) {
        // Averiguar cuantas columnas tendrá la lista dependiendo del tamaño de pantalla.
        var gridCrossAxisCount = sizingInfo.screenSize.width <= 1024 ? 2 : 4;
        return GridView.count(
            childAspectRatio: 0.8,
            mainAxisSpacing: 2.0,
            crossAxisSpacing: 2.0,
            padding: const EdgeInsets.all(kShopListPadding),
            crossAxisCount: gridCrossAxisCount,
            children: shopItemList.map((shopItemModel) {
              return GridTile(child: ShopItemCard(item: shopItemModel));
            }).toList());
      },
    );
  }
}

/// Shop ítem card.
class ShopItemCard extends StatelessWidget {
  final ShopItemModel item;
  const ShopItemCard({
    Key? key,
    required this.item,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Get Cart and app state by provider.
    final shopCart = Provider.of<Cart>(context);
    final appState = Provider.of<AppStateModel>(context, listen: false);
    // Price Formater.
    var pesosFormat = NumberFormat.currency(symbol: "\$");

    return ResponsiveBuilder(
      builder: (context, sizingInfo) {
        // Font size multiplier.
        var fontSizeMultiplier =
            ResponsiveHelper.textSizeMultiplier(sizingInfo);
        return Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
          child: InkWell(
            onTap: () {
              // Mostrar la página del producto.
              appState.detailPage = DetailPageConfiguration(
                key: 'ProductDetail',
                path: productDetailPath,
                uiPage: Pages.productDetail,
                id: item.id,
              );
            },
            child: Container(
              alignment: Alignment.center,
              child: Column(
                children: [
                  // Item image.
                  Expanded(
                    flex: 4,
                    child: SizedBox(
                      height: double.infinity,
                      width: double.infinity,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: FittedBox(
                          fit: BoxFit.cover,
                          clipBehavior: Clip.hardEdge,
                          child: FadeInImage.memoryNetwork(
                              image: item.imageURL,
                              placeholder: kTransparentImage,
                              imageErrorBuilder:
                                  (context, exception, stackTrace) {
                                return Image.asset('images/missing_image.png');
                              }),
                        ),
                      ),
                    ),
                  ),
                  // Item info.
                  Expanded(
                    flex: 3,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // Item title.
                          Text(
                            item.title,
                            style: TextStyle(
                              color: kTextColorCardTitle,
                              fontWeight: FontWeight.bold,
                              fontSize:
                                  kTextSizeShopItemTitle * fontSizeMultiplier,
                            ),
                          ),
                          // Material.
                          Row(
                            children: [
                              Text(
                                'Material: ',
                                style: TextStyle(
                                  color: kTextColorCardLabel,
                                  fontWeight: FontWeight.bold,
                                  fontSize: kTextSizeShopItemLabel *
                                      fontSizeMultiplier,
                                ),
                              ),
                              Text(
                                item.material,
                                style: TextStyle(
                                  color: kTextColorCardMaterial,
                                  fontWeight: FontWeight.bold,
                                  fontSize: kTextSizeShopItemLabel *
                                      fontSizeMultiplier,
                                ),
                              )
                            ],
                          ),
                          //
                          const SizedBox(height: 4.0),
                          // Item price.
                          Text(
                            pesosFormat.format(item.price),
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontSize:
                                  kTextSizeShopItemPrice * fontSizeMultiplier,
                              fontWeight: FontWeight.bold,
                              color: kTextColorShopItemPrice,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  // Add to cart button.
                  Expanded(
                    child: AddToCartButton(
                      item: item,
                      appState: appState,
                      shopCart: shopCart,
                      fontSizeMultiplier: fontSizeMultiplier,
                    ),
                  ),
                  Expanded(
                    child: ElevatedButton(
                      onPressed: () {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                content: Text("Producto ${item.id}"),
                              );
                            });
                      },
                      child: Text(
                        'Agregar al carrito',
                        style: TextStyle(
                          fontSize: kTextSizeShopItemAddToCartButton *
                              fontSizeMultiplier,
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}

class AddToCartButton extends StatelessWidget {
  const AddToCartButton({
    Key? key,
    required this.item,
    required this.appState,
    required this.shopCart,
    required this.fontSizeMultiplier,
  }) : super(key: key);

  final ShopItemModel item;
  final AppStateModel appState;
  final Cart shopCart;
  final double fontSizeMultiplier;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {
        if (item.stock > 0) {
          // Solo puedo agregar al carrito si el usuario esta logueado.
          if (appState.printUser.isSignedIn) {
            shopCart.addToCart(item);
          } else {
            Dialogs.showMessageDialog(
              context,
              message: const Text(
                'Debes iniciar sesión para agregar al carrito.',
              ),
            );
          }
        }
      },
      style: ElevatedButton.styleFrom(
        minimumSize: const Size(double.infinity, double.infinity),
      ),
      child: Text(
        'Agregar al carrito',
        style: TextStyle(
          fontSize: kTextSizeShopItemAddToCartButton * fontSizeMultiplier,
        ),
      ),
    );
  }
}
