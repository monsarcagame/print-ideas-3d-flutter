import 'package:flutter/material.dart';
import 'package:print_ideas_3d/models/core/app_state_model.dart';
import 'package:print_ideas_3d/widgets/templates/layout_template.dart';
import 'package:print_ideas_3d/router/ui_pages.dart';

// RouteInformationParser
class PrintAppRouteInformationParser
    extends RouteInformationParser<PageConfiguration> {
  @override
  Future<PageConfiguration> parseRouteInformation(
      RouteInformation routeInformation) async {
    final uri = Uri.parse(routeInformation.location!);

    // Handle '/' path.
    if (uri.pathSegments.isEmpty) {
      return PageHelper.homePageConfig;
    }

    // Handle length=1 paths.
    if (uri.pathSegments.length == 1) {
      switch (uri.pathSegments.first) {
        case 'admin':
          return PageHelper.adminLoginPageConfig;
        case 'login':
          return PageHelper.loginPageConfig;
        case 'sign_in':
          return PageHelper.signInPageConfig;
        case 'home':
          return PageHelper.homePageConfig;
        case 'works':
          return PageHelper.worksPageConfig;
        case 'services':
          return PageHelper.servicesPageConfig;
        case 'shop':
          return PageHelper.shopPageConfig;
        case 'cart':
          return PageHelper.cartPageConfig;
        case 'merchant_orders':
          return PageHelper.merchantOrdersPageConfig;
        case 'about':
          return PageHelper.aboutPageConfig;
        case 'contact':
          return PageHelper.contactPageConfig;
        default:
          return PageHelper.unknownPageConfig;
      }
    }

    // Handle two length paths
    if (uri.pathSegments.length == 2) {
      // 'service_detail/:id'
      if (uri.pathSegments[0] == 'service_detail') {
        var id = uri.pathSegments[1];

        return DetailPageConfiguration(
            key: 'ServiceDetail',
            path: serviceDetailPath,
            uiPage: Pages.serviceDetail,
            id: id);
      }
      // 'work_detail/:id'
      if (uri.pathSegments[0] == 'work_detail') {
        var id = uri.pathSegments[1];

        return DetailPageConfiguration(
            key: 'WorkDetail',
            path: workDetailPath,
            uiPage: Pages.workDetail,
            id: id);
      }
      // 'product_detail/:id'
      if (uri.pathSegments[0] == 'product_detail') {
        var id = uri.pathSegments[1];

        return DetailPageConfiguration(
            key: 'ProductDetail',
            path: productDetailPath,
            uiPage: Pages.productDetail,
            id: id);
      }
      // cart/checkout.
      if (uri.pathSegments[0] == 'cart' && uri.pathSegments[1] == 'checkout') {
        return PageHelper.checkoutPageConfig;
      }
      return PageHelper.unknownPageConfig;
    }

    return PageHelper.unknownPageConfig;
  }

  @override
  RouteInformation restoreRouteInformation(PageConfiguration configuration) {
    return RouteInformation(location: configuration.getPagePath());
  }
}

/// Main RouterDelegate.
class PrintAppRouterDelegate extends RouterDelegate<PageConfiguration>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<PageConfiguration> {
  // Global key.
  @override
  final GlobalKey<NavigatorState> navigatorKey;
  // App state.
  AppStateModel? _appState;

  PrintAppRouterDelegate() : navigatorKey = GlobalKey<NavigatorState>();

  set appState(AppStateModel newAppState) {
    if (_appState == newAppState) {
      return;
    }
    _appState = newAppState;
    _appState!.addListener(notifyListeners);
  }

  @override
  PageConfiguration get currentConfiguration {
    if (_appState == null) {
      return PageHelper.splashPageConfig;
    } else {
      // Si la página actual es de tipo detalle
      if (_appState!.detailPage != null) {
        return _appState!.detailPage as PageConfiguration;
      } else {
        if (_appState!.currentPage == Pages.splash) {
          return PageHelper.splashPageConfig;
        } else if (_appState!.currentPage == Pages.adminLogin) {
          return PageHelper.adminLoginPageConfig;
        } else if (_appState!.currentPage == Pages.home) {
          return PageHelper.homePageConfig;
        } else if (_appState!.currentPage == Pages.login) {
          return PageHelper.loginPageConfig;
        } else if (_appState!.currentPage == Pages.signIn) {
          return PageHelper.signInPageConfig;
        } else if (_appState!.currentPage == Pages.works) {
          return PageHelper.worksPageConfig;
        } else if (_appState!.currentPage == Pages.services) {
          return PageHelper.servicesPageConfig;
        } else if (_appState!.currentPage == Pages.shop) {
          return PageHelper.shopPageConfig;
        } else if (_appState!.currentPage == Pages.cart) {
          return PageHelper.cartPageConfig;
        } else if (_appState!.currentPage == Pages.checkout) {
          return PageHelper.checkoutPageConfig;
        } else if (_appState!.currentPage == Pages.merchantOrders) {
          return PageHelper.merchantOrdersPageConfig;
        } else if (_appState!.currentPage == Pages.about) {
          return PageHelper.aboutPageConfig;
        } else if (_appState!.currentPage == Pages.contact) {
          return PageHelper.contactPageConfig;
        } else if (_appState!.currentPage == Pages.unknown) {
          return PageHelper.unknownPageConfig;
        }
      }
    }

    return PageHelper.homePageConfig;
  }

  @override
  Future<void> setNewRoutePath(PageConfiguration configuration) async {
    // Actualizo App State para reflejar la nueva ruta.
    switch (configuration.key) {
      case 'Unknown':
        _appState!.currentPage = Pages.unknown;
        break;
      case 'Home':
        _appState!.currentPage = Pages.home;
        break;
      case 'AdminLogin':
        _appState!.currentPage = Pages.adminLogin;
        break;
      case 'Works':
        _appState!.currentPage = Pages.works;
        break;
      case 'Services':
        _appState!.currentPage = Pages.services;
        break;
      case 'Shop':
        _appState!.currentPage = Pages.shop;
        break;
      case 'Cart':
        _appState!.currentPage = Pages.cart;
        break;
      case 'Checkout':
        _appState!.currentPage = Pages.checkout;
        break;
      case 'Merchant Orders':
        _appState!.currentPage = Pages.merchantOrders;
        break;
      case 'About':
        _appState!.currentPage = Pages.about;
        break;
      case 'Contact':
        _appState!.currentPage = Pages.contact;
        break;
      case 'ServiceDetail':
        _appState!.detailPage = configuration as DetailPageConfiguration;
        break;
      case 'WorkDetail':
        _appState!.detailPage = configuration as DetailPageConfiguration;
        break;
      case 'ProductDetail':
        _appState!.detailPage = configuration as DetailPageConfiguration;
        break;
      case 'Login':
        _appState!.currentPage = Pages.login;
        break;
      case 'SignIn':
        _appState!.currentPage = Pages.signIn;
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: navigatorKey,
      pages: const [
        MaterialPage(
            child: LayoutTemplate(
          child: Text('Child'),
        ))
      ],
      onPopPage: (route, result) {
        if (!route.didPop(result)) {
          return false;
        }

        notifyListeners();

        return true;
      },
    );
  }
}
