import 'package:flutter/material.dart';
import 'package:print_ideas_3d/models/core/app_state_model.dart';
import 'package:print_ideas_3d/views/about/about_view.dart';
import 'package:print_ideas_3d/views/authentication/login_admin_view.dart';
import 'package:print_ideas_3d/views/contact/contact_view.dart';
import 'package:print_ideas_3d/views/home/home_view.dart';
import 'package:print_ideas_3d/views/authentication/login_view.dart';
import 'package:print_ideas_3d/views/authentication/signup_view.dart';
import 'package:print_ideas_3d/views/services/service_detail.dart';
import 'package:print_ideas_3d/views/services/services_view.dart';
import 'package:print_ideas_3d/views/shop/add_product_view.dart';
import 'package:print_ideas_3d/views/shop/cart_view.dart';
import 'package:print_ideas_3d/views/shop/checkout_view.dart';
import 'package:print_ideas_3d/views/shop/merchant_orders_view.dart';
import 'package:print_ideas_3d/views/shop/product_view.dart';
import 'package:print_ideas_3d/views/shop/shop_view_embedded.dart';
import 'package:print_ideas_3d/views/splash/splash_view.dart';
import 'package:print_ideas_3d/views/error/unknown/unknown_page_view.dart';
import 'package:print_ideas_3d/views/works/work_detail_view.dart';
import 'package:print_ideas_3d/views/works/works_view.dart';
import 'package:provider/provider.dart';
import 'ui_pages.dart'
    show PageHelper, PageConfiguration, Pages, DetailPageConfiguration;

/// Secondary RouterDelegate.
class InnerRouterDelegate extends RouterDelegate<PageConfiguration>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<PageConfiguration> {
  /// Global key.
  @override
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  /// List of pages to represent the current state of the app.
  final List<Page> _pages = [];

  InnerRouterDelegate() {
    _replaceAll(PageHelper.homePageConfig);
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<AppStateModel>(builder: (context, appState, child) {
      // Construyo la lista de páginas en base a la app state.
      _constructPagesList(appState);

      return Navigator(
        key: navigatorKey,
        pages: List.of(_pages),
        onPopPage: (Route<dynamic> route, result) {
          appState.currentPage = Pages.home;
          _pages.remove(route.settings);
          notifyListeners();
          return route.didPop(result);
        },
      );
    });
  }

  @override
  Future<void> setNewRoutePath(PageConfiguration configuration) async {
    // This is not required for inner router delegate because it does not
    // parse route
    throw UnimplementedError();
  }

  @override
  Future<bool> popRoute() {
    if (_pages.length > 1) {
      _removePage(_pages.last as MaterialPage<dynamic>);
      return Future.value(true);
    }
    return Future.value(false);
  }

  /// Construye una lista de páginas en base al estado de la aplicación.
  void _constructPagesList(AppStateModel appState) {
    // Si no es una página de detalle.
    if (appState.detailPage == null) {
      switch (appState.currentPage) {
        case Pages.adminLogin:
          _replaceAll(PageHelper.adminLoginPageConfig);
          break;
        case Pages.login:
          _replaceAll(PageHelper.loginPageConfig);
          break;
        case Pages.signIn:
          _replaceAll(PageHelper.signInPageConfig);
          break;
        case Pages.home:
          _replaceAll(PageHelper.homePageConfig);
          break;
        case Pages.works:
          _replaceAll(PageHelper.worksPageConfig);
          break;
        case Pages.services:
          _replaceAll(PageHelper.servicesPageConfig);
          break;
        case Pages.shop:
          _replaceAll(PageHelper.shopPageConfig);
          break;
        case Pages.addProduct:
          if (_pages.isEmpty ||
              (_pages.last.arguments as PageConfiguration).uiPage ==
                  Pages.shop) {
            _addPage(PageHelper.addProductPageConfig);
          } else {
            _replaceAll(PageHelper.addProductPageConfig);
          }
          break;
        case Pages.cart:
          _replaceAll(PageHelper.cartPageConfig);
          break;
        case Pages.merchantOrders:
          _replaceAll(PageHelper.merchantOrdersPageConfig);
          break;
        case Pages.checkout:
          if (_pages.isEmpty ||
              (_pages.last.arguments as PageConfiguration).uiPage ==
                  Pages.cart) {
            _addPage(PageHelper.checkoutPageConfig);
          } else {
            _replaceAll(PageHelper.checkoutPageConfig);
          }
          break;
        case Pages.about:
          _replaceAll(PageHelper.aboutPageConfig);
          break;
        case Pages.contact:
          _replaceAll(PageHelper.contactPageConfig);
          break;
        case Pages.unknown:
          _replaceAll(PageHelper.unknownPageConfig);
          break;
        default:
          _replaceAll(PageHelper.homePageConfig);
      }
    } else {
      // Una página de detalle la agreo encima sin importar lo que haya.
      _addPage(appState.detailPage);
    }
  }

  /// Crea una MaterialPage a partir de una PageConfiguration.
  MaterialPage _createPage(Widget child, PageConfiguration pageConfig) {
    return MaterialPage(
        child: child,
        key: Key(pageConfig.key) as LocalKey?,
        name: pageConfig.path,
        arguments: pageConfig);
  }

  /// Eliminar una página de la lista de páginas.
  void _removePage(MaterialPage page) {
    if (_pages.remove(page)) {
      notifyListeners();
    }
  }

  /// Agrega una página a la lista _pages con su correspondiente widget.
  void _addPageData(Widget child, PageConfiguration pageConfig) {
    _pages.add(_createPage(child, pageConfig));
  }

  /// Agregar una página en base a una PageConfiguration.
  void _addPage(PageConfiguration? pageConfig) {
    // verifico si se puede agregar la page.
    final shouldAddPage = _pages.isEmpty ||
        (_pages.last.arguments as PageConfiguration).uiPage !=
            pageConfig!.uiPage;

    if (shouldAddPage) {
      //
      switch (pageConfig!.uiPage) {
        case Pages.unknown:
          _addPageData(const UnknownPageView(), pageConfig);
          break;
        case Pages.adminLogin:
          _addPageData(const LoginAdminView(), PageHelper.adminLoginPageConfig);
          break;
        case Pages.splash:
          _addPageData(const SplashView(), PageHelper.splashPageConfig);
          break;
        case Pages.login:
          _addPageData(const LoginView(), PageHelper.loginPageConfig);
          break;
        case Pages.signIn:
          _addPageData(const SignInView(), PageHelper.signInPageConfig);
          break;
        case Pages.about:
          _addPageData(const AboutView(), PageHelper.aboutPageConfig);
          break;
        case Pages.contact:
          _addPageData(const ContactView(), PageHelper.contactPageConfig);
          break;
        case Pages.home:
          _addPageData(const HomeView(), PageHelper.homePageConfig);
          break;
        case Pages.services:
          _addPageData(const ServicesView(), PageHelper.servicesPageConfig);
          break;
        case Pages.serviceDetail:
          _addPageData(
              ServiceDetail(
                  serviceId: (pageConfig as DetailPageConfiguration).id),
              pageConfig);
          break;
        case Pages.shop:
          _addPageData(const ShopViewEmbedded(), PageHelper.shopPageConfig);
          break;
        case Pages.addProduct:
          _addPageData(const AddProductView(), PageHelper.addProductPageConfig);
          break;
        case Pages.productDetail:
          _addPageData(
              ProductDetailView(
                  productId: (pageConfig as DetailPageConfiguration).id),
              pageConfig);
          break;
        case Pages.cart:
          _addPageData(const CartView(), PageHelper.cartPageConfig);
          break;
        case Pages.checkout:
          _addPageData(const CheckoutView(), PageHelper.checkoutPageConfig);
          break;
        case Pages.merchantOrders:
          _addPageData(
              const MerchantOrdersView(), PageHelper.merchantOrdersPageConfig);
          break;
        case Pages.works:
          _addPageData(const WorksView(), PageHelper.worksPageConfig);
          break;
        case Pages.workDetail:
          _addPageData(
              WorkDetailView(
                printingWorkId: (pageConfig as DetailPageConfiguration).id,
              ),
              pageConfig);
          break;
        default:
          break;
      }
    }
  }

  // Modifying the Contents:
  // Remove the last page and replaces it with the new page using the add method
  void replace(PageConfiguration newRoute) {
    if (_pages.isNotEmpty) {
      _pages.removeLast();
    }
    _addPage(newRoute);
  }

  /// Limpia la lista de páginas y agrega una nueva.
  void _replaceAll(PageConfiguration newPage) {
    _pages.clear();
    _addPage(newPage);
  }

  /// This is like the addPage method, but with a different name to be in sync with Flutter’s push and pop naming.
  void push(PageConfiguration newRoute) {
    _addPage(newRoute);
    notifyListeners();
  }

  /// Allows adding a new widget using the argument of type Widget.
  void pushWidget(Widget child, PageConfiguration newRoute) {
    _addPageData(child, newRoute);
    notifyListeners();
  }

  // Clears the entire navigation stack and adds all the new pages provided as the argument
  void setPath(List<MaterialPage> path) {
    _pages.clear();
    _pages.addAll(path);
    notifyListeners();
  }
}
