import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:print_ideas_3d/router/ui_pages.dart';
import 'package:print_ideas_3d/views/about/about_view.dart';
import 'package:print_ideas_3d/views/authentication/login_view.dart';
import 'package:print_ideas_3d/views/authentication/signup_view.dart';
import 'package:print_ideas_3d/views/contact/contact_view.dart';
import 'package:print_ideas_3d/views/services/service_detail.dart';
import 'package:print_ideas_3d/views/services/services_view.dart';
import 'package:print_ideas_3d/views/shop/shop_view.dart';
import 'package:print_ideas_3d/views/works/work_detail_view.dart';
import 'package:print_ideas_3d/views/works/works_view.dart';
import 'package:print_ideas_3d/widgets/templates/layout_template.dart';

import '../views/home/home_view.dart';

class RouterHelper {
  static GoRouter goRouter(
      {required GlobalKey<NavigatorState> rootNavigatorKey}) {
    return GoRouter(
      navigatorKey: rootNavigatorKey,
      routes: [
        ShellRoute(
          builder: (context, state, child) {
            return LayoutTemplate(
              child: child,
            );
          },
          routes: [
            GoRoute(
              name: Pages.home.name,
              path: homePath,
              builder: (context, state) => const HomeView(),
            ),
            GoRoute(
              name: Pages.login.name,
              path: loginPath,
              builder: (context, state) => const LoginView(),
            ),
            GoRoute(
              name: Pages.signIn.name,
              path: signInPath,
              builder: (context, state) => const SignInView(),
            ),
            GoRoute(
              name: Pages.works.name,
              path: worksPath,
              builder: (context, state) => const WorksView(),
              routes: <RouteBase>[
                GoRoute(
                  name: Pages.workDetail.name,
                  path: 'details/:id',
                  builder: (context, state) => WorkDetailView(
                      printingWorkId: state.pathParameters['id']!),
                )
              ],
            ),
            GoRoute(
              name: Pages.services.name,
              path: servicesPath,
              builder: (context, state) => const ServicesView(),
              routes: <RouteBase>[
                GoRoute(
                  name: Pages.serviceDetail.name,
                  path: 'detail/:id',
                  builder: (context, state) =>
                      ServiceDetail(serviceId: state.pathParameters['id']!),
                )
              ],
            ),
            GoRoute(
              name: Pages.shop.name,
              path: shopPath,
              builder: (context, state) => const ShopView(),
            ),
            GoRoute(
              name: Pages.about.name,
              path: aboutPath,
              builder: (context, state) => const AboutView(),
            ),
            GoRoute(
              name: Pages.contact.name,
              path: contactPath,
              builder: (context, state) => const ContactView(),
            ),
          ],
        )
      ],
    );
  }
}
