/// PageConfiguration class.
class PageConfiguration {
  final String key;
  final String path;
  final Pages uiPage;
  final bool isUnknown;

  const PageConfiguration({
    required this.key,
    required this.path,
    required this.uiPage,
    this.isUnknown = false,
  });

  const PageConfiguration.unknown()
      : isUnknown = true,
        key = 'Unknown',
        path = unknownPath,
        uiPage = Pages.unknown;

  String getPagePath() {
    return path;
  }
}

/// PageConfiguration subclass for detail pages.
class DetailPageConfiguration extends PageConfiguration {
  String id;

  DetailPageConfiguration(
      {required String key,
      required String path,
      required Pages uiPage,
      required this.id})
      : super(key: key, path: path, uiPage: uiPage);

  // Devuelve el path con el id incluido. por ej: /work_detail/1
  @override
  String getPagePath() {
    return '$path/$id';
  }
}

class PageHelper {
  // Page configurations. Algunas páginas predefinidas.
  static const PageConfiguration splashPageConfig =
      PageConfiguration(key: 'Splash', path: splashPath, uiPage: Pages.splash);

  static const PageConfiguration adminLoginPageConfig = PageConfiguration(
      key: 'AdminLogin', path: adminLoginPath, uiPage: Pages.adminLogin);

  static const PageConfiguration loginPageConfig =
      PageConfiguration(key: 'Login', path: loginPath, uiPage: Pages.login);

  static const PageConfiguration signInPageConfig =
      PageConfiguration(key: 'SignIn', path: signInPath, uiPage: Pages.signIn);

  static const PageConfiguration homePageConfig =
      PageConfiguration(key: 'Home', path: homePath, uiPage: Pages.home);

  static const PageConfiguration worksPageConfig =
      PageConfiguration(key: 'Works', path: worksPath, uiPage: Pages.works);

  static const PageConfiguration servicesPageConfig = PageConfiguration(
      key: 'Services', path: servicesPath, uiPage: Pages.services);

  static const PageConfiguration aboutPageConfig =
      PageConfiguration(key: 'About', path: aboutPath, uiPage: Pages.about);

  static const PageConfiguration contactPageConfig = PageConfiguration(
      key: 'Contact', path: contactPath, uiPage: Pages.contact);

// Shop pages.
  static const PageConfiguration shopPageConfig =
      PageConfiguration(key: 'Shop', path: shopPath, uiPage: Pages.shop);
  static const PageConfiguration addProductPageConfig = PageConfiguration(
      key: 'Add Product', path: addProductPath, uiPage: Pages.addProduct);
  static const PageConfiguration cartPageConfig =
      PageConfiguration(key: 'Cart', path: cartPath, uiPage: Pages.cart);
  static const PageConfiguration checkoutPageConfig = PageConfiguration(
      key: 'Checkout', path: checkoutPath, uiPage: Pages.checkout);
  static const PageConfiguration merchantOrdersPageConfig = PageConfiguration(
      key: 'Merchant Orders',
      path: merchantOrdersPath,
      uiPage: Pages.merchantOrders);

// Unknown page.
  static const PageConfiguration unknownPageConfig =
      PageConfiguration.unknown();
}

// Paths
const String splashPath = '/splash';
const String adminLoginPath = '/admin';
const String loginPath = '/login';
const String signInPath = '/sign_in';
const String homePath = '/';
const String worksPath = '/works';
const String workDetailPath = '/work_detail';
const String servicesPath = '/services';
const String serviceDetailPath = '/service_detail';
const String shopPath = '/shop';
const String productDetailPath = '/product_detail';
const String addProductPath = '/shop/add_product';
const String cartPath = '/cart';
const String checkoutPath = '/cart/checkout';
const String merchantOrdersPath = '/merchant_orders';
const String aboutPath = '/about';
const String contactPath = '/contact';
const String unknownPath = '/404';

// Enumera todas las Pages de la app.
enum Pages {
  splash('/splash'),
  adminLogin('/admin'),
  login('/login'),
  signIn('/sign_in'),
  home('/'),
  works('/works'),
  workDetail('/work_detail'),
  services('/services'),
  serviceDetail('/service_detail'),
  shop('/shop'),
  productDetail('/product_detail'),
  addProduct('/shop/add_product'),
  cart('/cart'),
  checkout('/cart/checkout'),
  merchantOrders('/merchant_orders'),
  about('/about'),
  contact('/contact'),
  unknown('/404');

  const Pages(this.path);
  final String path;
}
