import 'dart:typed_data';

import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:cloud_functions/cloud_functions.dart';
import 'package:print_ideas_3d/helpers/error_handling.dart';
import 'package:print_ideas_3d/models/core/user_model.dart';
import 'package:print_ideas_3d/models/shop/cart_model.dart';
import 'package:print_ideas_3d/helpers/mercadopago_helper.dart';
import 'package:print_ideas_3d/models/shop/merchant_order_model.dart';
import 'package:print_ideas_3d/models/shop/shop_item_model.dart';
import 'package:async/async.dart';
import 'package:print_ideas_3d/router/ui_pages.dart';

class FirebaseService {
  // Memoizer para obtener las categorias de productos una sola vez.
  final AsyncMemoizer<Map<String, List<String>>> _categoriesMemoizer =
      AsyncMemoizer<Map<String, List<String>>>();

  /// Crea un usuario con FirebaseAuth y guarda su info en FirebaseFirestore.
  Future<Either<Exception, User>> createUser(
      String name, String email, String password) async {
    try {
      UserCredential userCredential = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: email, password: password);

      User user = userCredential.user!;

      return Right(user);
    } on FirebaseAuthException catch (e) {
      return Left(e);
    }
  }

  /// Guarda la información de un usuario registrado con FirebaseAuth en FirebaseFirestore.
  Future<void> saveUserInfoToFirestore(User user, String name) async {
    return FirebaseFirestore.instance.collection('users').doc(user.uid).set(
        {'uid': user.uid, 'email': user.email, 'name': name, 'isAdmin': false});
  }

  /// Obtiene la información de un usario.
  Future<Map<String, dynamic>> getUserInfoFromFirestore(String uid) async {
    DocumentSnapshot<Map<String, dynamic>> userSnapshot;
    Map<String, dynamic> userInfo = Map.identity();
    CollectionReference users = FirebaseFirestore.instance.collection('users');

    try {
      userSnapshot =
          await users.doc(uid).get() as DocumentSnapshot<Map<String, dynamic>>;
      // Agrego toda la info del documento del usuario.
      userInfo.addAll(userSnapshot.data()!);
    } on FirebaseAuthException catch (e) {
      throw Failure(code: e.code, message: '${e.message}');
    }

    return Future.value(userInfo);
  }

  /// Autenticar mediante un vínculo de correo electrónico.
  Future<void> signInWithEmailAndLink(String email) async {
    var acs = ActionCodeSettings(
        url: 'http://printideas3d.online$loginPath', handleCodeInApp: true);
    try {
      await FirebaseAuth.instance
          .sendSignInLinkToEmail(email: email, actionCodeSettings: acs);
    } on FirebaseAuthException catch (e) {
      throw Failure(code: e.code, message: '${e.message}');
    }
  }

  /// Iniciar sesion con email y contraseña.
  Future<User?> signInUser(email, password) async {
    try {
      UserCredential userCredential = await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);

      return userCredential.user;
    } on FirebaseAuthException catch (e) {
      switch (e.code) {
        case 'network-request-failed':
          {
            throw Failure(
              code: e.code,
              message: 'Ha ocurrido un error de red',
            );
          }
        default:
          {
            throw Failure(
              code: e.code,
              message: 'Hubo un error al intentar autenticarse',
            );
          }
      }
    }
  }

  /// Google sign in.
  Future<UserCredential> signInWithGoogle() async {
    try {
      return await FirebaseAuth.instance
          .signInWithPopup(_getProviderForProviderId('google.com'));
    } on FirebaseAuthException catch (e) {
      throw Failure(code: e.code, message: e.message ?? 'Error');
    }
  }

  /// Facebook sign in.
  Future<UserCredential> signInWithFacebook() async {
    try {
      // Once signed in, return the UserCredential
      return await FirebaseAuth.instance
          .signInWithPopup(_getProviderForProviderId('facebook.com'));
    } on FirebaseAuthException catch (e) {
      // Manejar los diferentes errores.
      switch (e.code) {
        // El usuario esta iniciando sesión con otro proveedor.
        case 'account-exists-with-different-credential':
          throw SignInFailure(
            email: e.email ?? '',
            credential: e.credential ??
                const AuthCredential(
                    providerId: 'facebook.com', signInMethod: 'facebook.com'),
            code: 'account-exists-with-different-credential',
            message:
                'La cuenta está registrada con otro proveedor,\nintenta iniciar sesión con otro método.',
          );
        default:
          throw Failure(code: e.code, message: e.message ?? 'Error');
      }
    }
  }

  /// Construct provider from provider id.
  dynamic _getProviderForProviderId(String providerId) {
    switch (providerId) {
      case 'google.com':
        GoogleAuthProvider googleProvider = GoogleAuthProvider();
        googleProvider
            .addScope('https://www.googleapis.com/auth/contacts.readonly');
        googleProvider.setCustomParameters({'login_hint': 'user@example.com'});
        return googleProvider;
      case 'facebook.com':
        FacebookAuthProvider facebookProvider = FacebookAuthProvider();
        facebookProvider.addScope('email');
        facebookProvider.setCustomParameters({
          'display': 'popup',
        });
        return facebookProvider;
    }
  }

  /// Sign out.
  Future<void> signOutUser() async {
    await FirebaseAuth.instance.signOut();
  }

  /// Add shop item to firestore.
  Future<Either<String, FirebaseException>> addShopItem(
      ShopItemModel item) async {
    CollectionReference shopItems =
        FirebaseFirestore.instance.collection('shopItems');

    try {
      DocumentReference docRef = await shopItems.add(item.itemData());

      return Left(docRef.id);
    } on FirebaseException catch (e) {
      return Right(e);
    }
  }

  /// Obtener de firestore las categorias y subcategorias disponibles.
  Future<Map<String, List<String>>> getCategories() {
    return _categoriesMemoizer.runOnce(() async {
      Map<String, List<String>> categories = {};
      CollectionReference categoriesRef =
          FirebaseFirestore.instance.collection('categories');

      try {
        QuerySnapshot categoriesSnapshot = await categoriesRef.get();

        for (var docSnapshot in categoriesSnapshot.docs) {
          // El nombre de la categoria es el id del documento.
          String categoryName = docSnapshot.id;
          List<String> subcategories =
              List.from(docSnapshot.get('subcategories'));
          categories.putIfAbsent(categoryName, () => subcategories);
        }

        return categories;
      } on FirebaseException {
        rethrow; // Ver como usar el plugin 'catcher' para gestionar errores y excepciones.
      }
    });
  }

  /// Get shop items collection stream. Los items del stream tienen un stock mayor a cero.
  Stream<QuerySnapshot> getShopItemsByDateStream() {
    return FirebaseFirestore.instance
        .collection('shopItems')
        .where('stock', isGreaterThan: 0)
        .snapshots();
  }

  /// Obtener shop items ordenados por fecha de creación.
  Future<Either<List<ShopItemModel>, Exception>> getShopItemsByDate() async {
    List<ShopItemModel> shopItemList = [];
    CollectionReference shopItemsCR =
        FirebaseFirestore.instance.collection('shopItems');

    try {
      QuerySnapshot itemQuerySnapshot =
          await shopItemsCR.orderBy('publishedDate').get();

      for (var doc in itemQuerySnapshot.docs) {
        doc.reference.snapshots();
        Map<String, dynamic> data = doc.data() as Map<String, dynamic>;
        shopItemList.add(
          ShopItemModel(
            id: doc.id,
            title: data['title'],
            shortInfo: data['shortInfo'],
            description: data['description'],
            publishedDate: data['publishedDate'],
            price: data['price'],
            url: data['url'],
            imageURL: data['imageURL'],
            thumbnailURL: data['thumbnailURL'],
            status: data['status'],
            category: ShopItemCategory(
                name: data['category'], subcategory: data['subcategory']),
            mass: data['mass'],
            printingTime: data['printingTime'],
            material: data['material'],
            color: data['color'],
            height: double.parse(data['height']),
            length: double.parse(data['length']),
            width: double.parse(data['width']),
          ),
        );
      }
      return Left(shopItemList);
    } on FirebaseException catch (e) {
      return Right(e);
    }
  }

  /// Actualizar el stock de un item.
  Future<Either<bool, Exception>> updateShopItemStock(
      ShopItemModel item, int increment) async {
    DocumentReference documentReference =
        FirebaseFirestore.instance.collection('users').doc(item.id);

    try {
      await documentReference
          .update({'stock': FieldValue.increment(increment)});

      return const Left(true);
    } on FirebaseException catch (e) {
      return Right(e);
    }
  }

  /// Upload raw image to firebase storage.
  Future<Either<String, Exception>> uploadImage(
      Uint8List imageData,
      String? imagePath,
      void Function(firebase_storage.TaskSnapshot) listener) async {
    // Upload task.
    firebase_storage.UploadTask task = firebase_storage.FirebaseStorage.instance
        .ref(imagePath)
        .putData(imageData);
    // Escuchar eventos de la tarea.
    task.snapshotEvents.listen(listener, onError: (e) {});

    try {
      await task;

      // Get image url.
      var imageURL = await task.snapshot.ref.getDownloadURL();

      return Left(/*Image.network(imageURL)*/ imageURL);
    } on FirebaseException catch (e) {
      // e.g, e.code == 'canceled'
      return Right(e);
    }
  }

  /// Mercadopago cloud function para obtener una preferencia.
  Future<String?> getMercadoPagoPreference(PrintUser user, Cart cart) async {
    // Si es un usuario logueado, creo la orden de compras.
    if (user.isSignedIn) {
      HttpsCallable callable =
          FirebaseFunctions.instance.httpsCallable('createPreference');
      CollectionReference ordersCollection =
          FirebaseFirestore.instance.collection('merchantOrders');

      try {
        HttpsCallableResult preferenceResult = await callable
            .call(MercadoPagoHelper.createPreferenceFromCart(cart));
        // Antes de retornar la preferencia, creo una orden de compra en la base de datos.
        await ordersCollection.doc(preferenceResult.data['preference_id']).set(
            MerchantOrderModel.fromCart(
                    id: preferenceResult.data['preference_id'],
                    userId: user.uid,
                    cart: cart,
                    orderCreationDate: Timestamp.now())
                .orderData());

        return preferenceResult.data['init_point'];
      } on FirebaseException catch (e) {
        throw Failure(code: e.code, message: '${e.message}');
      }
    }
    return null;
  }
}
