enum Flavor { dev, production }

class AppConfig {
  AppConfig({
    required this.buildFlavor,
  });

  final Flavor buildFlavor;
}
