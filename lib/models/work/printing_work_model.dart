import 'package:cloud_firestore/cloud_firestore.dart';

class PrintingWorkModel {
  String? id;
  String title;
  String shortInfo;
  String description;
  String imageURL;
  Timestamp printingWorkDate;
  String material;

  PrintingWorkModel(
      {required this.title,
      required this.shortInfo,
      required this.description,
      required this.imageURL,
      required this.printingWorkDate,
      required this.material});

  PrintingWorkModel.fromJSON(
      {required String this.id, required Map<String, dynamic> data})
      : title = data['title'] ?? 'Unknown title',
        shortInfo = data['shortInfo'] ?? 'Unknown short info',
        description = data['description'] ?? 'Unknown description',
        imageURL = data['imageURL'] ?? 'http://Unknown.png',
        printingWorkDate = data['printingWorkDate'] ?? Timestamp.now(),
        material = data['material'] ?? 'Unknown material';
}
