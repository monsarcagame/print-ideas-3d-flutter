import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:print_ideas_3d/models/shop/cart_model.dart';

/// Estado del pago de una orden.
enum PaymentStatus { opened, closed, expired }

///
class MerchantOrderItem {
  String shopItemId;
  double shopItemPrice;
  String shopItemThumbnailURL;
  int quantity;

  MerchantOrderItem(
      {required this.shopItemId,
      required this.shopItemPrice,
      required this.shopItemThumbnailURL,
      required this.quantity});

  Map<String, dynamic> toJSON() {
    return {
      'shopItemId': shopItemId,
      'shopItemPrice': shopItemPrice,
      'shopItemThumbnailURL': shopItemThumbnailURL,
      'quantity': quantity
    };
  }
}

/// Modelo para las ordenes de compras que realiza un cliente.
///
class MerchantOrderModel {
  String id;

  /// Identificador del usuario al cual le pertenece la orden de compra.
  String userId;

  /// Estado de la orden.
  PaymentStatus paymentStatus;

  /// Items con sus respectivas cantidades.
  List<MerchantOrderItem> items = [];

  /// Información de envío.
  ShippingInformation shipping;

  Timestamp orderCreationDate;

  MerchantOrderModel(
      {required this.id,
      required this.userId,
      required this.paymentStatus,
      required this.items,
      required this.shipping,
      required this.orderCreationDate});

  /// Crear una orden en base a un carrito de compras.
  ///
  MerchantOrderModel.fromCart(
      {required this.id,
      required this.userId,
      this.paymentStatus = PaymentStatus.opened,
      required Cart cart,
      required this.orderCreationDate})
      : shipping = cart.shippingInfo {
    // Items.
    items.addAll(cart.itemsWithQuantities.entries.map((shopItemModelEntry) =>
        MerchantOrderItem(
            shopItemId: shopItemModelEntry.key.id,
            shopItemPrice: shopItemModelEntry.key.price,
            shopItemThumbnailURL: shopItemModelEntry.key.thumbnailURL,
            quantity: shopItemModelEntry.value)));
  }

  /// Transformo la orden de compra en un mapa estilo JSON.
  Map<String, dynamic> orderData() {
    return {
      'userId': userId,
      'paymentStatus': paymentStatus.toString(),
      'items': items.map((merchantOrderItem) => merchantOrderItem.toJSON()),
      'shipping': shipping.shippingToJSON(),
      'orderCreationDate': orderCreationDate
    };
  }
}
