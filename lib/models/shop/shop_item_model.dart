import 'package:cloud_firestore/cloud_firestore.dart';

/// Current availability of the item in the store.
enum ProductStatus {
  inStock,
  availableForOrder,
  outOfStock,
  discontinued,
}

const itemAvailability = {
  ProductStatus.inStock: "in stock",
  ProductStatus.availableForOrder: "available for order",
  ProductStatus.outOfStock: "out of stock",
  ProductStatus.discontinued: "discontinued",
};

/// Representa a una categoria y su respectiva subcategoria del shop item.
class ShopItemCategory {
  final String? name;
  final String? subcategory;

  ShopItemCategory({required this.name, this.subcategory});

  @override
  String toString() {
    return '${name!} -> ${subcategory!}';
  }

  String get categoryPath => '${name!} -> ${subcategory!}';
}

/// Representa un producto en el shopping y se corresponde con la información de la base de datos.
class ShopItemModel {
  final String id;
  String title;
  String? shortInfo;

  /// Fecha en la que se publicó el producto.
  Timestamp publishedDate;

  /// Descripción del item.
  String description;

  /// Precio del item.
  double price;

  /// URL del item.
  String url;

  /// Imagenes del item.
  String imageURL;
  String thumbnailURL;

  /// Define el estado del producto. (in stock; available for order; preorder; out of stock; discontinued)
  String status;

  /// Representa categoria y subcategoria.
  ShopItemCategory category;

  /// Stock disponible del producto.
  int stock;

  /// Color principal del producto.
  String color;

  /// Material con el que está impreso el producto.
  String material;

  /// Tiempo de impresión [h].
  double printingTime;

  /// Masa de la pieza en [kg].
  double mass;

  /// Dimensiones de la pieza [m].
  double width, length, height;

  ShopItemModel(
      {required this.id,
      required this.title,
      this.shortInfo,
      required this.publishedDate,
      required this.description,
      required this.price,
      required this.url,
      required this.imageURL,
      required this.thumbnailURL,
      required this.status,
      required this.category,
      this.stock = 0,
      required this.color,
      this.material = 'PLA',
      required this.printingTime,
      required this.mass,
      required this.height,
      required this.length,
      required this.width});

  /// Construye el shop item model a partir de datos en formato JSON.
  ShopItemModel.fromJSON({required this.id, required Map<String, dynamic> data})
      : title = data['title'] ?? 'Default title',
        price = data['price'] ?? 1.0,
        thumbnailURL = data['thumbnailURL'],
        shortInfo = data['shortInfo'] ?? 'Default short info.',
        publishedDate = data['publishedData'] ?? Timestamp.now(),
        description = data['description'] ?? 'Default description',
        url = data['url'] ?? 'https://printideas3d.online/#/product_detail/$id',
        imageURL = data['imageURL'],
        status = data['status'],
        category = ShopItemCategory(
            name: data['category'], subcategory: data['subcategory']),
        stock = data['stock'] ?? 1,
        color = data['color'] ?? 'ffffff',
        material = data['material'] ?? 'PLA',
        printingTime = data['printingTime'] ?? 1.0,
        mass = data['mass'] ?? 1.0,
        width = data['width'] ?? 1.0,
        length = data['length'] ?? 1.0,
        height = data['height'] ?? 1.0;

  /// Get shop item data.
  Map<String, dynamic> itemData() {
    return {
      'title': title,
      'shortInfo': shortInfo,
      'publishedDate': publishedDate,
      'description': description,
      'price': price,
      'url': url,
      'imageURL': imageURL,
      'thumbnailURL': thumbnailURL,
      'status': status,
      'category': category.name,
      'subcategory': category.subcategory,
      'stock': stock,
      'color': color,
      'material': material,
      'printingTime': printingTime,
      'mass': mass,
      'width': width,
      'length': length,
      'height': height
    };
  }

  /// JSON-LD for microdata.
  Map<String, dynamic> toJson() => {
        "@context": "https://schema.org",
        "@type": "Product",
        'productID': id,
        'name': title,
        'description': description,
        'url': '',
        'image': imageURL,
        'brand': 'Print.Ideas3D',
        'offers': [
          {
            "@type": "Offer",
            'price': price,
            'priceCurrency': 'ARS',
            'itemCondition': "https://schema.org/NewCondition",
            'availability': "https://schema.org/InStock"
          }
        ]
      };

  //
  @override
  bool operator ==(other) => other is ShopItemModel && other.id == id;

  //
  @override
  int get hashCode => id.hashCode;
}
