import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:print_ideas_3d/helpers/shipping_helpers.dart';
import 'package:print_ideas_3d/models/shop/shop_item_model.dart';

/// LocalPickup: El comprador retira la compra en el local del vendedor.
/// SucursalPickup: El comprador retira el envió por sucursal de correo.
/// HomeDelivery: El comprador recibe el pedido en su domicilio.
enum ShippingMode { localPickup, sucursalPickup, homeDelivery }

/// Estados en los que se puede encontrar un envío.
enum ShippingStatus { pending, delivered }

/// Direccíon básica.
class Address {
  String contact;
  String zipCode;
  String street;
  int number;
  String state;
  String city;

  Address(
      {required this.contact,
      required this.zipCode,
      required this.street,
      required this.number,
      required this.state,
      required this.city});

  /// Construct from JSON data.
  Address.fromJSON(data)
      : contact = data['contact'] ?? '',
        zipCode = data['zipCode'] ?? '',
        street = data['street'] ?? '',
        number = data['number'] ?? 1799,
        state = data['state'] ?? '',
        city = data['city'] ?? '';

  /// Dirección en formato JSON.
  Map<String, dynamic> addressData() {
    return {
      'contact': contact,
      'zipCode': zipCode,
      'street': street,
      'number': number,
      'state': state,
      'city': city
    };
  }
}

/// Dirección completa con correo electrónico.
class CompleteAddress extends Address {
  String? floor;
  String? apartment;
  String email;

  CompleteAddress(
      {required String contact,
      required String zipCode,
      required String street,
      required int number,
      required String state,
      required String city,
      required this.floor,
      required this.apartment,
      required this.email})
      : super(
            contact: contact,
            zipCode: zipCode,
            street: street,
            number: number,
            state: state,
            city: city);

  /// Construct from JSON data.
  CompleteAddress.fromJSON(data)
      : floor = data['floor'],
        apartment = data['apartment'],
        email = data['email'],
        super.fromJSON(data);

  @override
  Map<String, dynamic> addressData() {
    Map<String, dynamic> completeData = {
      'floor': floor,
      'apartment': apartment,
      'email': email
    };
    completeData.addAll(super.addressData());
    return completeData;
  }
}

/// Información de envío.
class ShippingInformation {
  Address origin;
  CompleteAddress? destination;
  double weight;
  double shippingCost;
  ShippingMode shippingMode;
  ShippingStatus status;

  /// Contructor que inicia la información de envío en modo 'LocalPickup' y estado 'Pending'.
  ShippingInformation(
      {required this.origin,
      this.destination,
      this.weight = 0.0,
      this.shippingCost = 0.0,
      this.shippingMode = ShippingMode.localPickup,
      this.status = ShippingStatus.pending});

  /// Validar la información de envío.
  bool validate() =>
      (shippingMode != ShippingMode.localPickup && destination != null) ||
      shippingMode == ShippingMode.localPickup;

  /// Actualizar solo con los parametros que se pasan.
  void copyWith(
      {Address? origin,
      CompleteAddress? destination,
      double? weight,
      shippingMode}) {
    this.origin = origin ?? this.origin;
    this.destination = destination ?? this.destination;
    this.weight = weight ?? this.weight;
    this.shippingMode = shippingMode ?? this.shippingMode;
  }

  /// Transforma la información de envío en un mapa estilo JSON.
  Map<String, dynamic> shippingToJSON() {
    return {
      'origin': origin.addressData(),
      'destination': destination?.addressData(),
      'weight': weight,
      'shippingCost': shippingCost,
      'shippingMode': shippingMode.toString(),
      'shippingStatus': status.toString()
    };
  }

  /// Reiniciar la información de envío.
  void reset() {
    destination = null;
    weight = 0.0;
    shippingCost = 0.0;
    shippingMode = ShippingMode.localPickup;
    status = ShippingStatus.pending;
  }
}

/// Carrito de compras.
///
/// ChangeNotifier para que los cambios en el carrito se puedan escuchar.
class Cart with ChangeNotifier {
  final Map<ShopItemModel, int> _items;
  final ShippingInformation _shippingInfo;

  Cart({required Address shippingOrigin})
      : _items = {},
        _shippingInfo = ShippingInformation(origin: shippingOrigin) {
    FirebaseAuth.instance.userChanges().listen(_onUserChanged);
  }

  /// Verificar que el estado del carrito sea apto para checkout.
  bool validateForCheckout() => !isEmpty && _shippingInfo.validate();

  /// Función que escucha cambios en el usuario.
  void _onUserChanged(User? user) {
    if (user != null) {
      // Cargar el carrito para el usuario logueado.
    } else {
      // Si el usuario cerró sesión, reseteo el carrito.
      resetCart();
    }
  }

  /// Resetear el carrito de compras.
  void resetCart() {
    _items.clear();
    _shippingInfo.reset();

    notifyListeners();
  }

  /// Obtener la lista de items, sin tener en cuenta la cantidad de cada uno.
  List<ShopItemModel> get items => _items.keys.toList();

  /// Mapa de items con sus respectivas cantidades.
  Map<ShopItemModel, int> get itemsWithQuantities => _items;

  /// Precio total del carrito sin tener en cuenta el envío.
  double get total {
    return _items.entries.fold(
        0.0,
        (previousValue, entry) =>
            previousValue + entry.key.price * entry.value);
  }

  /// Obtiene el precio de envío del carrito.
  double get shippingCost => _shippingInfo.shippingCost;

  /// Precio total del carrito, incluyendo el envío.
  double get totalWithShipping {
    return total + shippingCost;
  }

  /// Cantidad de items en el carrito.
  int get itemCount {
    return _items.entries
        .fold(0, (previousValue, entry) => previousValue + entry.value);
  }

  /// Is cart empty?.
  bool get isEmpty => _items.isEmpty;

  /// Get shipping info.
  ShippingInformation get shippingInfo => _shippingInfo;

  /// Peso total del carrito.
  double get cartWeight => _items.keys
      .fold(0.0, (previousValue, shopItem) => previousValue + shopItem.mass);

  /// Actualizar información de envío.
  Future<void> updateShippingInformation(
      {Address? newOrigin,
      CompleteAddress? newDestination,
      double? newWeight,
      ShippingMode? newShippingMode}) async {
    _shippingInfo.copyWith(
        origin: newOrigin,
        destination: newDestination,
        weight: newWeight,
        shippingMode: newShippingMode);
    // Actualizo el precio del envío.
    _shippingInfo.shippingCost = (_shippingInfo.destination != null &&
            newShippingMode != ShippingMode.localPickup)
        ? await PaqArShippingHelper.getShippingPrice(_shippingInfo)
        : 0.0;

    notifyListeners();
  }

  /// Obtener la cantidad de productos del mismo tipo.
  int? getItemCount(ShopItemModel item) {
    if (_items.containsKey(item)) {
      return _items[item];
    }

    return 0;
  }

  /// Obtener el subtotal de un item.
  double subtotalItem(ShopItemModel item) {
    if (_items.containsKey(item)) {
      return item.price * _items[item]!;
    }

    return 0.0;
  }

  /// Agregar un item al carrito. Si el ítem ya existe, incrementa la cantidad.
  void addToCart(ShopItemModel item) {
    _items.update(item, (value) => value + 1, ifAbsent: () => 1);

    notifyListeners();
  }

  /// Eliminar un item del carrito.
  void removeFromCart(ShopItemModel item) {
    if (_items.containsKey(item)) {
      _items.remove(item);

      notifyListeners();
    }
  }

  /// Decrement item count.
  void decrementItemCount(ShopItemModel cartItem) {
    if (_items.containsKey(cartItem)) {
      _items.update(cartItem, (value) => value > 0 ? value - 1 : 0);

      notifyListeners();
    }
  }

  /// Increment item count.
  void incrementItemCount(ShopItemModel cartItem) {
    if (_items.containsKey(cartItem)) {
      _items.update(cartItem, (value) => value + 1);

      notifyListeners();
    }
  }
}
