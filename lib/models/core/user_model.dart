/// Mantiene la información de un usuario registrado en la app.
class PrintUser {
  bool isSignedIn;
  String uid;
  String name;
  String email;
  // Para mas seguridad este campo es solo para la construcción de la UI.
  // En el backend se verifica realmente si es un admin.
  bool isAdmin;

  PrintUser({this.uid = '', this.name = '', this.email = ''})
      : isSignedIn = false,
        isAdmin = false;

  void resetUser() {
    isSignedIn = false;
    uid = '';
    name = 'Nameless';
    email = 'unknown@mail.com';
    isAdmin = false;
  }
}
