import 'package:flutter/material.dart';

class DrawerStateModel extends ChangeNotifier {
  bool _isDrawerOpened = false;

  bool get isDrawerOpened => _isDrawerOpened;

  set isDrawerOpened(bool isOpened) {
    _isDrawerOpened = isOpened;
    notifyListeners();
  }
}
