import 'package:print_ideas_3d/models/shop/cart_model.dart';

class BusinessModel {
  final String displayName;
  final String email;
  // Social.
  final String whatsappNumber;
  final String facebookPage;
  final String instagramPage;

  // Dirección donde se encuentra el negocio.
  final Address address;

  // App url.
  final String appUrl;

  // About text.
  final String aboutText;

  // Modelo 3d de la app.
  final String model3dId;
  final String model3dUrl;

  // Shop
  final String shopUrl;

  BusinessModel(
      this.displayName,
      this.email,
      this.whatsappNumber,
      this.facebookPage,
      this.instagramPage,
      this.address,
      this.appUrl,
      this.aboutText,
      this.model3dId,
      this.model3dUrl,
      this.shopUrl);

  BusinessModel.fromJSON(data)
      : displayName = data['displayName'] ?? 'Print Ideas 3D',
        email = data['email'] ?? '',
        whatsappNumber = data['whatsappNumber'] ?? '',
        facebookPage = data['facebookPage'] ?? '',
        instagramPage = data['instagramPage'] ?? '',
        address = Address.fromJSON(data['address']),
        appUrl = data['appUrl'] ?? '',
        aboutText = data['aboutText'] ?? '',
        model3dId = data['model3dId'] ?? '',
        model3dUrl = data['model3dUrl'] ??
            'https://modelviewer.dev/shared-assets/models/Astronaut.glb',
        shopUrl = data['shopUrl'] ?? 'https://printideas3d.empretienda.com.ar/';
}
