import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:print_ideas_3d/models/core/user_model.dart';
import 'package:print_ideas_3d/router/ui_pages.dart';

/// Clase que mantiene el estado de la aplicación en coordinación con eventos externos y del usuario.
class AppStateModel extends ChangeNotifier {
  bool _isLoading = false;
  Pages _currentPage;
  DetailPageConfiguration? _detailPageConfiguration;
  final PrintUser _printUser;

  AppStateModel()
      : _currentPage = Pages.home,
        _printUser = PrintUser() {
    // Escuchar todos los cambios en el estado de autenticación.
    FirebaseAuth.instance.authStateChanges().listen(_updateUserFromStream);
  }

  /// Función que va a escuchar los cabios del usuario.
  void _updateUserFromStream(User? user) async {
    if (user != null) {
      var idTokenResult = await user.getIdTokenResult(true);

      _printUser.uid = user.uid;
      _printUser.email = user.email ?? 'anonimo@mail.com';
      _printUser.name = user.displayName ?? 'Anónimo';
      _printUser.isAdmin = idTokenResult.claims?['isAdmin'] ?? false;
      _printUser.isSignedIn = true;
    } else {
      _printUser.resetUser();
    }

    notifyListeners();
  }

  /// Obtener la página que se esta mostrando actualmente.
  Pages get currentPage => _currentPage;

  /// Set current page.
  set currentPage(Pages page) {
    _currentPage = page;
    // En este caso no habria detail page,
    // entonces queda null para que el Router Delegate no lo tenga en cuenta
    // cuando genera la lista de routes.
    _detailPageConfiguration = null;

    notifyListeners();
  }

  /// Obtener current detail page.
  DetailPageConfiguration? get detailPage => _detailPageConfiguration;

  /// Set detail page.
  set detailPage(DetailPageConfiguration? newDetail) {
    _detailPageConfiguration = newDetail;

    notifyListeners();
  }

  /// Indica si la app esta en un estado de carga.
  bool get isLoading => _isLoading;

  /// Obtener FirestoreUser.
  PrintUser get printUser => _printUser;

  /// Set isLoading.
  set isLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }
}
