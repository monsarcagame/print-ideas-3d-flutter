class ServiceModel {
  String? id;
  String? name;
  String? description;
  List<String>? images;

  ServiceModel({this.id, this.name, this.description, this.images});

  ServiceModel.fromJSON({String? id, required Map<String, dynamic> data})
      : id = data['id'],
        name = data['name'],
        description = data['description'],
        images = List.from(
          data['images'],
        );
}
