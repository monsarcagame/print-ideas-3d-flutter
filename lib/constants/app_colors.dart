import 'package:flutter/material.dart';

const Color kThemePrimaryColor = Color(0xFF303030);
const Color kPrimaryColor = Color(0xFFa0e040);
const Color kSecondaryColor = Color(0xFFd61b9e);
// Navigation bars.
const Color kTopNavigationBarColor = Color(0xFF212121);
const Color kTopNavigationBarForegroundColor = kPrimaryColor;
// Drawer.
const Color kDrawerColor = Colors.black54;
const Color kDrawerBackgroundHeaderColor = Color.fromRGBO(153, 218, 53, 1);
// Buttons.
const Color kButtonPrimaryColor = Colors.black;
// Inputs.
const Color kTextFormFieldColor = kSecondaryColor;
// Shop cards.
const Color kCardBackgroundColor = Colors.white;
