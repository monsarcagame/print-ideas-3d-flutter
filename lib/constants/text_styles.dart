import 'package:flutter/material.dart';
import 'package:print_ideas_3d/constants/app_colors.dart';

// Text colors.
const kTextColorDefault = Colors.white;
const kTextColorMainTheme = Color(0xFF98DA34);
const kTextColorNavBar = Color(0xFFE8E8E8);
const kTextColorCallToAction = Color(0xFF171717);
const kTextColorHomeIntroduction = kTextColorMainTheme;
const kTextColorLogo = Color(0xFFE8E8E8);
const kTextColorHovering = Color(0xFF98DA34);
const kTextFormFieldColor = Color(0xFFE8E8E8);
// Design text sizes. Estas constantes son la base para calcular el tamaño de fuente para otras pantallas.
const kTextSizeDefault = 18.0;
const kTextSizeNavbarPage = 18.0;
const kTextSizeAuth = 16.0;
const kTextSizeCallToAction = 16.0;
const kTextSizeHomeIntroduction = 19.0;
const kTextSizeLogo = 16.0;
// Nav bar.
const kTextStyleNavBar = TextStyle(
    fontSize: 18, fontWeight: FontWeight.normal, color: Color(0xFFE8E8E8));
const kTextStyleCallToAction = TextStyle(
    fontSize: 16, fontWeight: FontWeight.w800, color: Color(0xFF171717));
const kTextStyleHomeIntroduction = TextStyle(
    fontSize: 22, fontWeight: FontWeight.w500, color: Color(0xFFE8E8E8));
const kTextStyleLogo = TextStyle(
    fontSize: 16, fontWeight: FontWeight.w500, color: Color(0xFFE8E8E8));
// Generic card.
const double kTextSizeCardTitle = 18.0;
const double kTextSizeCardLabel = 18.0;
const double kTextSizeCardDefault = 14.0;
const Color kTextColorCardTitle = Color(0xFF98DA34);
const Color kTextColorCardLabel = Colors.white;
const Color kTextColorCardDefault = Colors.white;
const kTextColorCardMaterial = kSecondaryColor;
// Shop item card.
const kTextSizeShopItemTitle = 20.0;
const kTextSizeShopItemLabel = 16.0;
const kTextSizeShopItemShortInfo = 16.0;
const kTextSizeShopItemPrice = 18.0;
const kTextSizeShopItemAddToCartButton = 16.0;
const kTextColorShopItemPrice = Colors.lightGreen;
// Forms.
const kTextFormFieldStyle = TextStyle(color: kTextFormFieldColor);
const kTextFormLabelStyle = TextStyle(color: kSecondaryColor);
// InputDecorator.
const kInputDecoratorHelperTextStyle = TextStyle(color: Colors.amber);
const kInputDecoratorHintTextStyle = TextStyle(color: kSecondaryColor);
