import 'package:flutter/material.dart';

//
const double kBodyHPadding = 8.0;
const double kDesktopNavbarHeight = 80.0;
// Forms.
const kFormPadding = EdgeInsets.all(8.0);
const kFormFieldPadding = EdgeInsets.symmetric(horizontal: 25.0, vertical: 5.0);

// Shop constansts
const kShopListPadding = .0;
const kShopItemCardPadding = 8.0;

// Navbar style.
const double kNavbarSidePadding = 10.0;
