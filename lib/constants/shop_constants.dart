import 'package:print_ideas_3d/models/shop/cart_model.dart';

final CompleteAddress kDefaultOriginAddress = CompleteAddress(
    contact: 'PrintIdeas3D',
    zipCode: '7167',
    street: 'Mons',
    number: 1799,
    state: 'Buenos Aires',
    city: 'Pinamar',
    floor: '',
    apartment: '',
    email: 'print.ideas3d@gmail.com');
final CompleteAddress kDefaultDestinationAddress = CompleteAddress(
    contact: 'default user',
    zipCode: '7167',
    street: 'street',
    number: 1111,
    state: 'Buenos Aires',
    city: 'Pinamar',
    floor: '',
    apartment: '',
    email: 'default@address.com');
