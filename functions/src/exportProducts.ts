/* eslint-disable require-jsdoc */
import {google} from "googleapis";
import serviceAccount = require("../sheets_updater_service_account.json");

const sheets = google.sheets("v4");

export enum SpreadsheetOperation {create, update, delete}

const jwtClient = new google.auth.JWT({
  email: serviceAccount.client_email,
  key: serviceAccount.private_key,
  scopes: ["https://www.googleapis.com/auth/spreadsheets"],
});

const jwtAuthPromise = jwtClient.authorize();

// Obtener todos los productos de la hoja de cálculo.
async function getAllValues(): Promise<any[][]> {
  try {
    const result = await sheets.spreadsheets.values.get({
      auth: jwtClient,
      spreadsheetId: "1ovPyXx9CRATtrLfOXdjMitCnIwrrXm1yIstfrAtXu5k",
      range: "catalog_products!A3:A",
    });
    const rows = result.data.values ?? [];
    return rows;
  } catch (e: any) {
    console.log(e.message);
    return [];
  }
}

// Busca un producto en la hoja de calculo y en caso de existir
// devuelve su rango.
async function findProduct(productId: string):
    Promise<{exists: boolean, range: string}> {
  console.log(`productId: ${productId}`);
  try {
    // Obtengo todos los productos de la hoja de cálculo.
    const rows = await getAllValues();
    // Obtengo la cantidad de productos.
    const rowCount = rows?.length ?? 0;
    console.log(`Values: ${rows?.toLocaleString()}`);
    console.log(`Row Count: ${rowCount}`);
    // Busco el indice.
    const rowIndex = rows?.length ?
      rows.findIndex((element) => element[0].toString() == productId) : -1;
    return rowIndex >= 0 ?
      {exists: true, range: `catalog_products!A${rowIndex+3}:I${rowIndex+3}`} :
      {exists: false, range: `catalog_products!A${rowCount+3}:I${rowCount+3}`};
  } catch (e: any) {
    console.log(e.message);
    return {exists: false, range: "catalog_products!A3:I3"};
  }
}

// Actualiza la hoja de cálculo que representa el catalogo de productos.
export async function updateCatalogSpreadsheet(
    productAttributes:string[],
    operation: SpreadsheetOperation
) : Promise<void> {
  const finalData: Array<Array<string>> = [];
  finalData.push(productAttributes);

  await jwtAuthPromise;
  // Get product range. El id es el primer elemento del array productAttributes.
  const product = await findProduct(productAttributes[0]);
  console.log("Product info: " + product);

  if (!product.exists) {
    if (operation == SpreadsheetOperation.create ||
        operation == SpreadsheetOperation.update) {
      console.log("OPERATION: create");
      await sheets.spreadsheets.values.append({
        auth: jwtClient,
        spreadsheetId: "1ovPyXx9CRATtrLfOXdjMitCnIwrrXm1yIstfrAtXu5k",
        range: product.range,
        valueInputOption: "RAW",
        requestBody: {values: finalData, majorDimension: "ROWS"},
      }, {});
    }
  } else {
    if (operation == SpreadsheetOperation.create ||
        operation == SpreadsheetOperation.update) {
      console.log("OPERATION: update");
      await sheets.spreadsheets.values.update({
        auth: jwtClient,
        spreadsheetId: "1ovPyXx9CRATtrLfOXdjMitCnIwrrXm1yIstfrAtXu5k",
        range: product.range,
        valueInputOption: "RAW",
        requestBody: {values: finalData, majorDimension: "ROWS"},
      });
    } else {
      console.log("OPERATION: delete");
      await sheets.spreadsheets.values.clear({
        auth: jwtClient,
        spreadsheetId: "1ovPyXx9CRATtrLfOXdjMitCnIwrrXm1yIstfrAtXu5k",
        range: product.range,
        requestBody: {},
      });
    }
  }
}
