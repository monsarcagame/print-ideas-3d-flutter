import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import * as mercadopago from "mercadopago";
import express = require("express");
// eslint-disable-next-line max-len
// import {updateCatalogSpreadsheet, SpreadsheetOperation} from "./exportProducts";

// Firebase admin.
admin.initializeApp();

// Firestore database.
const db = admin.firestore();

// Funcion que se activa cuando se actualiza un item del shop.
exports.productOnCreate = functions.firestore.document("shopItems/{itemId}")
    .onWrite(async (change) => {
      /* // Obtengo la información necesaria para armar la fila del spreadsheet.
      const id = change.after.exists ? change.after.id : change.before.id;
      const title = change.after.exists ? change.after.data()?.title : "";
      const description =
        change.after.exists ? change.after.data()?.description : "";
      const availability =
        change.after.exists ? change.after.data()?.status : "";
      const condition = "new";
      const price =
        change.after.exists ? change.after.data()?.price + " ARS" : "";
      const link = change.after.exists ? change.after.data()?.url : "";
      const imageLink =
        change.after.exists ? change.after.data()?.imageURL : "";
      const brand = "Print.Ideas3D";
      const googleProductCategory = "536"; // Home & Garden
      const additionalImageLink = imageLink;
      const color = change.after.exists ? change.after.data()?.color : "Blanco";
      const material =
        change.after.exists ? change.after.data()?.material : "PLA";
      // const maximumWeight =
      // change.after.exists ? change.after.data()?.mass + " kg" : "100 g";

      const productAttributes: string[] = [
        id, title, description, availability, condition,
        price, link, imageLink, brand, googleProductCategory,
        additionalImageLink, color, material,
      ];
      // Averiguo el tipo de operación realizada (create, update, delete).
      const operation: SpreadsheetOperation =
        !change.before.exists ? SpreadsheetOperation.create :
          change.after.exists ? SpreadsheetOperation.update :
                                SpreadsheetOperation.delete;
      // Actualizo la hoja de cálculo.
      try {
        await updateCatalogSpreadsheet(productAttributes, operation);
      } catch (error) {
        throw new functions.https.HttpsError("internal", "Hubo un error");
      }*/
    });

// Funcion que escucha la creacion de documentos en la coleccion "admins".
// Cuando se crea un admin, se agregan un custom claims de administrador.
exports.adminOnCreate = functions.firestore.document("admins/{adminId}")
    .onCreate(async (snapshot) => {
      await admin.auth().setCustomUserClaims(
          snapshot.data().userId, {isAdmin: true});

      return {result: "Custom claim isAdmin agregada correctamente."};
    });

// Función que se activa cuando se crea un nuevo usuario.
exports.authOnCreate = functions.auth.user().onCreate(async (user) => {
  console.log("Creating document for user ${user.uid}");
  // Crear un documento para el usuario en la coleccion "users".
  try {
    await admin.firestore().collection("users").doc(user.uid).set({
      email: user.email,
      emailVerified: user.emailVerified,
      displayName: user.displayName,
    });
  } catch (error) {
    throw new functions.https.HttpsError("internal", "Hubo un error");
  }
});

// Función que se activa cuando hago la petición http modelViewer.
exports.modelViewer = functions.https.onRequest(async (request, response) => {
  // Obtengo el id del ShopItemModel para obtener la url del modelo 3d.
  const modelId = request.query.modelId;
  // Obtengo la url del modelo 3d a traves de la base de datos firestore.
  const modelDoc = db.doc("3dModels/" + modelId);
  try {
    const modelSnap = await modelDoc.get();
    // Creo el html document con el tag <model-viewer>
    const html = `
      <!DOCTYPE html>
      <html>
          <body>
              <style>
                html {
                  height: 100%;
                }
                body {
                  margin: 0;
                  height: 100%;
                }
                model-viewer {
                  width: 100%;
                  height: 100%;
                  --poster-color: transparent;
                  --progress-mask: transparent;
                  --progress-bar-color:${modelSnap.get("progressBarColor")};
                }
              </style>
              <model-viewer 
                  bounds="tight"
                  src="${modelSnap.get("src")}"
                  poster="${modelSnap.get("poster")}"
                  ar ar-modes="webxr scene-viewer quick-look"
                  camera-controls
                  camera-orbit="45deg 55deg 100%"
                  disable-zoom
                  auto-rotate
                  environment-image="${modelSnap.get("environmentImage")}"
                  shadow-intensity="2"               
              >
              </model-viewer>
                <div class="progress-bar hide" slot="progress-bar">
                  <div class="update-bar"></div>
                </div>
              <!-- Import the component -->
              <script type="module" src="https://unpkg.com/@google/model-viewer/dist/model-viewer.min.js"></script>
          </body>
      </html>
    `;
    response.send(html);
  } catch (error) {
    throw new functions.https.HttpsError("internal", `Hubo un error al intentar
    leer desde la base de datos`);
  }
});

// MERCADOPAGO FUNCTIONS //

// Credenciales de Mercadopago.
mercadopago.configure({
  // eslint-disable-next-line max-len
  access_token: "APP_USR-5936167231893102-042801-7d134835d516f8f4f84e7b818d82acb3-750614012",
});

// Dada la información de un producto o servicio, obtener una preferencia.
exports.createPreference = functions.https.onCall(async (data) => {
  // Items.
  const items = data.items;
  // Crea un objeto de preferencia en base a los datos recibidos
  // en la solicitud.
  const preference = {
    items: items,
    shipments: data.shipments,
  };

  try {
    const res = await mercadopago.preferences.create(preference);
    return {
      "status": "success",
      "preference_id": res.body.id,
      "init_point": res.body.init_point,
    };
  } catch (error) {
    return {"status": "failure"};
  }
});

// Receptor de notificaciones de mercadopago.
const app = express();

app.post("/", async (req, res) => {
  let merchantOrder;
  if (req.body["topic"] == "merchant_order") {
    try {
      // Obtengo la orden de compra a través de la api de mercado pago.
      merchantOrder =
        await mercadopago.merchant_orders.findById(req.body["id"]);

      switch (merchantOrder.body["status"]) {
        case "opened": {
          // Actualizo la orden de compra en firestore.
          const doc = "merchantOrders/" + merchantOrder.body["preference_id"];
          db.doc(doc).set({"paymentStatus": "Opened"}, {"merge": true});
          break;
        }
        case "closed": {
          // Actualizo la orden de compra en firestore.
          const doc = "merchantOrders/" + merchantOrder.body["preference_id"];
          db.doc(doc).set({"paymentStatus": "Closed"}, {"merge": true});
          break;
        }
        case "expired": {
          // Actualizo la orden de compra en firestore.
          const doc = "merchantOrders/" + merchantOrder.body["preference_id"];
          db.doc(doc).set({"paymentStatus": "Expired"}, {"merge": true});
          break;
        }
        default: {
          res.status(404).send("ERROR");
          break;
        }
      }
    } catch (error) {
      res.status(404).send(error);
    }
  }
  // Respuesta para validar que se recibio correctamente la notificación.
  res.status(200).send("OK");
});

exports.handleMercadopagoIPN = functions.https.onRequest(app);
